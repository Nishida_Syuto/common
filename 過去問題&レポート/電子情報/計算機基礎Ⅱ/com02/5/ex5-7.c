#include<stdio.h>

int kazu(int x, int y);

int main(){
	int x,y;

	printf("1個目の数字を入力して下さい:");
	scanf("%d",&x);
	printf("2個目の数字を入力して下さい");
	scanf("%d",&y);

	printf("%dと%dの最大公約数は%dです\n",x,y,kazu(x,y));

	return(0);
}

int kazu(int x,int y){
	if(x>y){
		if(x%y!=0) kazu(y,x%y);
		else return y;
	}
	else{
		if(y%x!=0) kazu(x,y%x);
		else return x;
	}
}
