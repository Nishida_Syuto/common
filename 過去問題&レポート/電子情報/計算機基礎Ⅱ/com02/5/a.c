#include <stdio.h>

int gcd(int a, int b);

main()
{
    int x, y, z;

    scanf("%d", &x);
    scanf("%d", &y);

    z = gcd(x, y);

    printf("%d\n", z);
}

int gcd(int a, int b)
{
    int d;

    if (b == 0)
      d = a;
    else
      d = gcd(b, a % b);

    return d;
}
