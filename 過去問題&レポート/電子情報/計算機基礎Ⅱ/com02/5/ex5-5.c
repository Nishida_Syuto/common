#include<stdio.h>

int keta(int a);	//与えられた桁の数を全て足して返す関数

int main(){
	int a;

	printf("数を教えて下さい:");
	scanf("%d",&a);

	printf("\n%dの各桁を足すと%dです.\n",a,keta(a));
	return(0);
}


int keta(int a){
	if(a<10)return(a);
	return a % 10 + keta(a / 10);
}
