#include <stdio.h>

int factorial(int kazu);//階乗を返す関数


int main(){
	int a;

	printf("数を入力して下さい:");
	scanf("%d",&a);

	printf("\n %dの階乗は,%dです.\n",a,factorial(a));
	return(0);
}


int factorial(int a){
	if(a == 1)return(1);
	return a * factorial(a-1);
}
