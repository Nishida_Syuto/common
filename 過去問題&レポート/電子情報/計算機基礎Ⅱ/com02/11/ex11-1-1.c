#include<stdio.h>

int a=1;

int &a;

void func1(){
	a=3;
	printf("(func1)変数aの中身は,%dです.\n",a);
	ptintf("変数aを保管しているアドレスは,%pです.\n",&a);
}

int main(){
	a=2;
	printf("(main)変数aの中身は,%dです.\n",a);
	ptintf("変数aを保管しているアドレスは,%pです.\n",&a);

	func1();

	return(0);
}

