#include<stdio.h>
#include<string.h>
#define TRUE 1


struct status{
        char name[13];
        int hp;
        //int mp;
        int offense;
        int defense;
        int money;
};
struct status monster_data[]={{"スライム",10,1,1,3},
		{"王女",5,3,1,100},
		{"ラスボス",999,100,100,100}};
char message[80];

void print_message(char message[]){
        printf("%s\n",message);
        sleep(1);
}

int attack(int offense_power,int defense_power,\
		char offense_name[],char defense_name[]){
		int attack;
        attack = offense_power - defense_power;
        if(attack < 0)attack = 0;
        sprintf(message,"%sの攻撃.\n%sに%dのダメージを与えた.",\
                        offense_name,defense_name,attack);
        print_message(message);
        return attack;
}

int main(){
        struct status player = {"勇者",10,2,1,100};     //名前は全角4文字
        struct status monster;
        int number;
        srand((unsigned int)time(NULL));       //乱数の初期化

        //戦闘開始
        while(player.hp > 0){
                //敵の選択
                number = rand()%3;
                monster = monster_data[number];
                strcpy(monster.name,monster_data[number].name);

                sprintf(message,"%sが現れた!",monster.name);
                print_message(message);

                //戦闘開始(面倒なので必ず勇者のターンから始める)
                while(TRUE){
                        //本当ならここに「コマンドの選択」があるはず.
                        //今回は常に「戦う」を選択したプログラムを作る.
                      monster.hp -= attack(player.offense,monster.defense,\
                                        player.name,monster.name);
                        if(monster.hp <= 0){
                                sprintf(message,"%sは%sを倒した.\n%dゴールド見つけた.\n",\
                                        player.name,monster.name,monster.money);
                                print_message(message);
                                player.money += monster.money;
                                break;
                        }

                        //敵のターン
                        //こちらも,「戦う」だけを用意
                        player.hp -= attack(monster.offense,player.defense,\
                                        monster.name,player.name);
                        if(player.hp <= 0){
                                sprintf(message,"%sは%sに倒されてしまった.",\
                                        player.name,monster.name);
			print_message(message);
                                break;
                        }
                }
        }
        return(0);
}

