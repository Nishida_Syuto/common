#include<stdio.h>

struct person
{
	char id[8];
	char name[20];
};

int main(){
	struct person elec_user[10];
	char name[][8]={"Taro","Ren","Sota","Futa",\
			"Hirota","Hanako","Yui","Hina","Haruna","Hinata"};

	int i;
	struct person *p;

	for(i=0;i<10;i++){
		sprintf(elec_user[i].id,"t150%3d",i + 100);
		sprintf(elec_user[i].name,"Ryukoku %s",name[i]);
	}

	p = elec_user;
	for(i=0;i<10;i++){
		printf("学籍番号%sの%sさんです.\n",(*p).id,p->name);
		p++;
	}
	return(0);
}
