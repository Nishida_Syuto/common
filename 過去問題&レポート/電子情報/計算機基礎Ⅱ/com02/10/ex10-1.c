#include<stdio.h>
#include<string.h>

struct person
{
	char id[8];
	char name[20];
	int bt_y;
	int bt_m;
	int bt_d;
};

struct person user1;

int main(){

	strcpy(user1.id,"t150345");
	strcpy(user1.name,"Ryukoku Taro");
	user1.bt_y = 1997;
	user1.bt_m = 4;
	user1.bt_d = 1;

	printf("学籍番号%sの%sさんは,西暦%d年%d月%d日生まれです.\n",\
		user1.id,user1.name,user1.bt_y,user1.bt_m,user1.bt_d);

	return(0);
}
