#include<stdio.h>

struct complex
{
	double real;			//実数部
	double imaginary;		//虚数部
};

struct complex complex_add(struct complex func_a,struct complex func_b){
	struct complex func_c;
	func_c.real = func_a.real + func_b.real;
	func_c.imaginary = func_a.imaginary + func_b.imaginary;
	return func_c;
}

void print_complex(char message[],struct complex a){
	printf("%s = %3lf + %3lfi\n",message,a.real,a.imaginary);
}


struct complex complex_引(struct complex func_a,struct complex func_b){
        struct complex func_d;
        func_d.real = func_a.real - func_b.real;
	func_d.imaginary = func_a.imaginary - func_b.imaginary;
        return func_d;
}

void print_complex(char message[],struct complex a){
        printf("%s = %3lf + %3lfi\n",message,a.real,a.imaginary);
}


struct complex complex_乗(struct complex func_a,struct complex func_b){
        struct complex func_e;
        func_e.real = func_a.real * func_b.real;
	func_e.imaginary = func_a.imaginary * func_b.imaginary;
        return func_e;
}

void print_complex(char message[],struct complex a){
        printf("%s = %3lf + %3lfi\n",message,a.real,a.imaginary);
}

struct complex complex_除(struct complex func_a,struct complex func_b){
        struct complex func_f;
        func_f.real = func_a.real / func_b.real;
	func_f.imaginary = func_a.imaginary / func_b.imaginary;
        return func_f;
}

void print_complex(char message[],struct complex a){
        printf("%s = %3lf + %3lfi\n",message,a.real,a.imaginary);
}






int main(){
	struct complex a,b,c,d,e,f;

	a.real = 10;a.imaginary = 5;
	b.real = 20;b.imaginary = 3;
	c = complex_add(a,b);
	d = complex_引(a,b);
	e = complex_乗(a,b);
	f = complex_除(a,b);


	print_complex("a",a);
	print_complex("b",b);
	print_complex("a + b",c);
	print_complex("a - b",d);
	print_complex("a * b",e);
	print_complex("a / b",f);

	return(0);
}

