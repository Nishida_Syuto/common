#include<stdio.h>

struct person
{
	char id[8];
	char name[20];
	int bt_y;
	int bt_m;
	int bt_d;
};

struct person elec_user[10];

int main(){
	int i;

	for(i=0;i<21;i++){
		sprintf(elec_user[i].id,"t150%3d",i + 100);
		sprintf(elec_user[i].name,"Ryukoku %c",i + 'a');
		elec_user[i].bt_y = 1997;
		elec_user[i].bt_m = 4;
		elec_user[i].bt_d = 1 + i;
	}

	for(i=0;i<21;i++){
		printf("学籍番号%sの%sさんは、西暦%d年%d月%d日生まれです.\n",\
			elec_user[i].id,elec_user[i].name,\
			elec_user[i].bt_y,elec_user[i].bt_m,elec_user[i].bt_d);
	}

	return(0);
}
