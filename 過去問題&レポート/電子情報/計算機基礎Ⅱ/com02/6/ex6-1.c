#include<stdio.h>

int MaxInt(int a,int b){
	if(a < b)return(b);//aとbが等しいときはaを返します.
	return(a);
}

int MinInt(int a,int b){
	if(a > b)return(b);//aとbが等しいときはaを返します.
	return(a);
}

int main(){
	int x = 10,y = 20;
	printf("%dと%dは,",x,y);
	printf("%dの方が大きく.%dの方が小さいです.\n",MaxInt(x,y),MinInt(x,y));
	return(0);
}
