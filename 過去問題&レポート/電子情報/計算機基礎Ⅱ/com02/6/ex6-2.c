#include<stdio.h>

double MaxInt(double a,double b){
        if(a < b)return(b);//aとbが等しいときはaを返します.
        return(a);
}

double MinInt(double a,double b){
        if(a > b)return(b);//aとbが等しいときはaを返します.
        return(a);
}

double main(){
        double x = 10,y = 20;
        printf("%fと%fは,",x,y);
        printf("%fの方が大きく.%fの方が小さいです.\n",MaxInt(x,y),MinInt(x,y));
        return(0);
}

