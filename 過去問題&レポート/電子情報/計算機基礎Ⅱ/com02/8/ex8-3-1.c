#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define GU	0
#define CHOKI	1
#define PA	2

int main(){
	int janken_com;
	int janken_hito;
	//毎回乱数が変わるようにします.
	srand((unsigned int)time(NULL));

	int i=0;
	while(1){
		i++;

		//それぞれの手を決めます.
		janken_com = rand()%3;
		printf("あなたの手を入力して下さい(ぐー=0,ちょき=1,ぱー=2):");
		scanf("%d",&janken_hito);

		printf("相手の手:%d\n",janken_com);

		if(
                (janken_com == GU       &&janken_hito == PA)||
                (janken_com == CHOKI    &&janken_hito == GU)||
                (janken_com == PA       &&janken_hito == CHOKI)
		) break;
	}

	//結果を判断します.
	printf("%d回目であなたが勝ちました.\n",i);
	return(0);
}
