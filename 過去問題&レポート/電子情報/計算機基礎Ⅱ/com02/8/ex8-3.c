#include<stdio.h>
//ループの条件を書き間違えると大変なことになります.
//いわゆる「無限ループ」です.
//このプログラムを終えるには,CTRL + Cを押して下さい.

int main(){
	int i=1;
	while(i<11){
		i=5;
	printf("iの値は%dです.\n",i);
	}
	return(0);
}
