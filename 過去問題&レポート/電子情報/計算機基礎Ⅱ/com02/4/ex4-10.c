#include<stdio.h>

int main(){
	int birthmonth;
	printf("あなたの生まれた月を入力してください:");
	scanf("%d",&birthmonth);

	printf("\n%d月は,",birthmonth);
	//四季に変換する.春は3-5月,夏は6-8月,秋は9-11月,冬は12-2月.

	//冬の場合
	if((birthmonth == 12) ||
	(birthmonth == 1) ||
	(birthmonth == 2))
		printf("冬です.\n");
	
	//春の場合
	if((birthmonth == 3) ||
	(birthmonth == 4) ||
	(birthmonth == 5))
		printf("春です.\n");
		
	//夏の場合
	if((birthmonth == 6) ||
	(birthmonth == 7) ||
	(birthmonth == 8))	
		printf("夏です.\n");

	//秋の場合
	if((birthmonth == 9) ||
	(birthmonth == 10) ||
	(birthmonth == 11))
	printf("秋です.\n");

	//理解不能
	if((birthmonth >= 13) ||
	(birthmonth <= 0))
	printf("理解できません.\n");

	return(0);
}
