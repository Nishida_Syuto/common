#include<stdio.h>

int main(){
	int birthmonth;
	printf("あなたの生まれた月を入力してください:");
	scanf("%d",&birthmonth);

	printf("\n%d月は,",birthmonth);
	switch(birthmonth){
	//四季に変換する.春は3-5月,夏は6-8月,秋は9-11月,冬は12-2月.
	case 12:
	case 1:
	case 2:
		printf("冬です.\n");
		
	case 3:
	case 4:
	case 5:
		printf("春です.\n");
		
	case 6:
	case 7:
	case 8:
		printf("夏です.\n");
		
	case 9:
	case 10:
	case 11:
		printf("秋です.\n");
		
	default:
		printf("理解できません.\n");
	}
	return(0);
}
