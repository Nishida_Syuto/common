#include<stdio.h>

void swap_a(int a,int b){
	int tmp;
	tmp = a;
	a = b;
	b = tmp;
}

void swap_b(int *a,int *b){
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

void swap_c(int *a,int *b){
	int *tmp;
	tmp = a;
	a = b;
	b = tmp;
}

void swap_print(int a,int b){
	printf("a=%d,b=%d\n",a,b);
}

int main(){
		int a,b;

		a=100;b=200;
		swap_print(a,b);

	printf("値渡しで関数に引数を受け渡した場合,");
	swap_a(a,b);
	swap_print(a,b);

	printf("参照渡しで関数に引数を受け渡した場合ー１,");
	swap_b(&a,&b);
	swap_print(a,b);

	printf("参照受け渡しで関数に引数を受け渡した場合ー２,");
	swap_c(&a,&b);
	swap_print(a,b);

	return(0);
}
