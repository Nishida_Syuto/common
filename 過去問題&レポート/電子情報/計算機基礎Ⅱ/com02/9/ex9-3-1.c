#include<stdio.h>



void swap_b(int *a,int *b){
        int tmp;
        tmp = *a;
        *a = *b;
        *b = tmp;
}

/*void swap_print(int data){
	printf("data[0]=%d,data[1]=%d\n",data[0],data[1]);

}*/
int main(){
                int data[2];
		data[0]=10;data[1]=20;
        swap_b(&data[0],&data[1]);
	 printf("data[0]=%d,data[1]=%d\n",data[0],data[1]);
      /*  printf("参照渡しで関数に引数を受け渡した場合ー１,");
        swap_b(&data[0],&data[1]);
        swap_print(data);
*/

        return(0);
}
