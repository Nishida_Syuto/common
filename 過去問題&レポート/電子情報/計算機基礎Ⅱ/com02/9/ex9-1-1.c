#include<stdio.h>

int main(){
        double *p; //整数型のメモリを示すポイントの宣言
        double x=1;

        p=&x;   //変数xのポイントをoに代入
        printf("変数xは,メモリ%pに保管されており,中身は%fです.\n",&x,x);
        printf("ポイントpの指すアドレスは%pで,メモリの中身は%fです.\n",p,*p);
        printf("ポイントp自身は,アドレス%pに保管されています.\n",&p);

        *p=100;
        printf("ポイントpの指すメモリの中身を変更すると,\
                変数xの中身は%fに変わります.\n",x);

        return(0);
}

