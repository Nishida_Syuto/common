#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 256

int main(void);

int main(void)
{
    char name1[MAXLINE], name2[MAXLINE];
    char buffer[MAXLINE];
    float age1, age2;

    printf("1人目の名前を入力してください。¥n");
    gets(name1);
    printf("年齢を入力してください。¥n",name1);
    gets(buffer);
    age1 = atoi(buffer);

    printf("2人目の名前を入力してください。¥n");
    gets(name2);
    printf("年齢を入力してください。¥n",name2);
    gets(buffer);
    age2 = atoi(buffer);

    printf("%sさんと%sさんの平均年齢は%0.1f歳です。¥n", name1, name2, (age1 + age2) / 2);


    return(0);
}

