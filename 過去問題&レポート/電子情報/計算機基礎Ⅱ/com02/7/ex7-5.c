#include<stdio.h>
//ループの条件を書き間違えると大変なことになります.
//いわゆる「無限ループ」です.
//このプラグラムを終えるには,CTRL+Cを押してください.

int main(){
		int i;
		for(i=1;i<11;i=5){
				printf("iの値は%dです.\n",i);
		}
		return(0);
}
