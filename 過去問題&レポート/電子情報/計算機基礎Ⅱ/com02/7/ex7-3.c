#include<stdio.h>
//forの中でしか変数を使わない場合は,
//forの中で宣言しても問題ありません.
//ただし,コンパイル時に-std=c99のオプションを要求されます.

int main(){
		for(int i=1;i<11;i++){
				printf("iの値は%dです.\n",i);
		}
		return(0);
}
