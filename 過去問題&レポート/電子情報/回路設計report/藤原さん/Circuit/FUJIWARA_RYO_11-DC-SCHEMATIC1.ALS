.ALIASES
Q_Q1            Q1(C=COLLECTOR B=BASE E=EMITTER )
V_Vcc           Vcc(+=N01662 -=0 )
R_Rc            Rc(1=COLLECTOR 2=N01662 )

R_R1            R1(1=BASE 2=N01662 )

R_R2            R2(1=0 2=BASE )

R_Re            Re(1=0 2=EMITTER )

R_RL            RL(1=0 2=OUTPUT )
C_C1            C1(1=INPUT 2=BASE )
C_C2            C2(1=COLLECTOR 2=OUTPUT )
V_Vin           Vin(+=INPUT -=0 )
_    _(Collector=COLLECTOR)
_    _(Input=INPUT)
_    _(Output=OUTPUT)
_    _(Emitter=EMITTER)
_    _(Base=BASE)
.ENDALIASES
