.ALIASES
R_R1            R1(1=BASE 2=N09863 )

V_Vcc           Vcc(+=N09863 -=0 )
C_CL            CL(1=0 2=OUTPUT )
Q_Q1            Q1(C=COLLECTOR B=BASE E=EMITTER )
R_Rc            Rc(1=COLLECTOR 2=N09863 )

R_Re            Re(1=0 2=EMITTER )

C_C2            C2(1=COLLECTOR 2=OUTPUT )
R_RL            RL(1=0 2=OUTPUT )
V_Vin           Vin(+=INPUT -=0 )
R_R2            R2(1=0 2=BASE )

C_C1            C1(1=INPUT 2=BASE )
_    _(Collector=COLLECTOR)
_    _(Output=OUTPUT)
_    _(Input=INPUT)
_    _(Base=BASE)
_    _(Emitter=EMITTER)
.ENDALIASES
