#include<stdio.h>
#define STACK_SIZE 100
#define NUMBER 1
#define PLUS 2
#define MINUS 3
#define MULT 4
#define DIV 5
#define OTHER 6

int stack[STACK_SIZE];
int pos,c,token,num;

void init_stack(void){
  printf("Stack size:%d\n",STACK_SIZE);
  pos=-1;
}

void push(int data){
  if(pos<STACK_SIZE-1)
    stack[++pos]=data;
  else
    printf("Error:Stack full!\n");
}

int pop(void){
  if(pos>=0)
    return stack[pos--];
  else
    printf("Error:Stack empty!\n");
}

int is_digit(int c){
  switch(c){
  case'0':case'1':case'2':case'3':case'4':
  case'5':case'6':case'7':case'8':case'9':
    return 1;
  default:
    return 0;
  }
}

int gettoken(void){
  do{
    c=getchar();
  }while(c==' '||c=='\n'||c=='\t');

  if(is_digit(c)){
    num=c-'0';
    while(1){
      c=getchar();
      if(is_digit(c))num=10*num+(c-'0');
      else break;
    }
    return NUMBER;
  }
  else if(c=='+')return PLUS;
 else if(c=='-')return MINUS;
 else if(c=='*')return MULT;
 else if(c=='/')return DIV;
 else if(c==EOF)return EOF;
 else return OTHER;
}

int main(void){
  int d;

  init_stack();

  do{
    token=gettoken();
    switch(token){
    case NUMBER:
      push(num);
      break;
    case PLUS:
      push(pop()+pop());
      break;
    case MINUS:
      d=pop();
      push(pop()-d);
      break;
    case MULT:
      push(pop()*pop());
      break;
    case DIV:
      d=pop();
      if(d==0)
	printf("Error:Zero divide!\n");
      else push(pop()/d);
      break;
    case OTHER:
      printf("Illegal character!\n");
      break;
    }
  }
  while(token!=EOF);

  if(pos==0)
    printf("Answer:%d\n",pop());
  else
    printf("Expression syntax error!\n");
  return 0;
}
