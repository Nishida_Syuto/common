#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct univ{
  char name[20];
  char loc[20];
};

  int main(int argc,char *argv[]){
    FILE *fp;
    char *filename;
    int i;
    struct univ x[7]={
      {"Ryukoku","Shiga"},
      {"Tokyo","Tokyo"},
      {"Stanford","California"},
      {"Harvard","Massachusetts"},
      {"Princeton","New Jersey"},
      {"Yale","Connecticut"},
      {"Columbia","New York"}
    };
    char y[50];

    filename=argv[1];
    if((fp=fopen(filename,"w"))==NULL){
      printf("Can't open %s.\n",filename);
      exit(1);
    }
    for(i=0;i<7;i++){
      fprintf(fp,"%s:%s\n",x[i].name,x[i].loc);
    }
    fclose(fp);
    strcpy(y,x[0].name);
    strcat(y,"-");
    strcat(y,x[0].loc);
    printf("%s %d\n",y,(int)strlen(y));
  }
