#include<stdio.h>
#include<stdlib.h>

void select_sort(int *d,int n){
  int i,j,min,t;
  for(i=1;i<n;i++){
    min=i;
    for(j=i+1;j<=n;j++){
      if(d[j]<d[min])min=j;
    }
    t=d[min];d[min]=d[i];d[i]=t;
  }
}

int main(int argc,char *argv[]){
  FILE *fp;
  int *data,num_of_data,i;

  num_of_data=atoi(argv[1]);
  data=(int *)malloc(sizeof(int)*(num_of_data+1));
  data[0]=-1;

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=num_of_data;i++){
    fscanf(fp,"%d\n",&data[i]);
  }
  fclose(fp);

  select_sort(data,num_of_data);
  for(i=1;i<=num_of_data;i++)printf("%d\n",data[i]);
  return 0;
}
