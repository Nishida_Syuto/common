#include<stdio.h>
#include<stdlib.h>

int *buff;

unsigned int bits(unsigned int x,int k,int j){
  return (x>>k)&~(~0<<j);
}

void radix_sort(int d[],int n){
  int pass,order[2],i,j;

  for(pass=0;pass<31;pass++){
    for(j=0;j<2;j++)order[j]=0;
    for(i=1;i<=n;i++){
      order[bits(d[i],pass,1)]++;
    }
    order[1]=order[0]+order[1];
    for(i=n;i>=1;i--){
      buff[order[bits(d[i],pass,1)]--]=d[i];
    }
    for(i=1;i<=n;i++)d[i]=buff[i];
  }
}


int main(int argc,char *argv[]){
  FILE *fp;
  int *d,n,i;

  n=atoi(argv[1]);
  d=(int *)malloc(sizeof(int)*(n+1));
  d[0]=-1;
  buff=(int *)malloc(sizeof(int)*(n+1));

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=n;i++)
    fscanf(fp,"%d\n",&d[i]);
  fclose(fp);

  radix_sort(d,n);
  for(i=1;i<=n;i++)
    printf("%d\n",d[i]);
  return 0;
}
