#include<stdio.h>

void str_rev(char s[],int k){
  int i,n;
  for(i=0;i<k/2;i++){
    n=s[i];
    s[i]=s[k-i-1];
    s[k-i-1]=n;
  }
}

main(){
  int i;
  char str[]="3A5BE";
  str_rev(str,5);
  for(i=0;i<5;i++) printf("%c",str[i]);
}
