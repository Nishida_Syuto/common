#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define M 257
#define CHARMAX 10000

struct item{
  char *id;
  int value;
};
static struct item table[M];

static int chartop=0;
static int charbtm=CHARMAX;
static char charheap[CHARMAX];

char *enterstring(char *s){
  char *cp;
  int i,j,len,result;

  cp=s;
  len=0;
  while(*cp++) len++;
  len++;
  if(charbtm-chartop<len){
    printf("identifier string buffer overflow.");
    exit(1);
  }
  result=chartop;
  j=chartop;
  chartop+=len;
  cp=s;
  for(i=0;i<len;i++)charheap[j++]=(*cp++);
  return(&charheap[result]);
}

void initialize(){
  int i;
  for(i=0;i<M;i++){
    table[i].id=" ";
  }
}

int hash(char *v){
  int x=0;
  while(*v) x=256*x+(*v++);
  if(x<0) x=-x;
  return(x%M);
}

void enter(char *id1,int info1){
  int x;

  x=hash(id1);
  while(strcmp(table[x].id," ")){
    x=(x+1)%M;
  }
  table[x].id=enterstring(id1);
  table[x].value=info1;
}

int search(char *id1){
  int x,y;
  x=hash(id1);
  y=x;
  while(strcmp(table[x].id,id1)){
    x=(x+1)%M;
    if(x==y){
      printf("not found.\n");
      return(-1);
    }
  }
  return(table[x].value);
}

main(){
  int t;

  initialize();

  enter("Copernicus",1473);
  printf("%d\t",hash("Copernicus"));
  enter("Galilei",1564);
  printf("%d\t",hash("Galilei"));
  enter("Newton",1643);
  printf("%d\t",hash("Newton"));
  enter("Maxwell",1831);
  printf("%d\t",hash("Maxwell"));
  enter("Einstein",1879);
  printf("%d\t",hash("Einstein"));
  enter("Heisenberg",1901);
  printf("%d\n\n",hash("Heisenberg"));

  t=search("Newton");
  printf("%d\n\n",t);

  t=search("Copernicus");
  printf("%d\n\n",t);

  t=search("Heisenberg");
  printf("%d\n\n",t);

  t=search("Pauli");
  printf("%d\n",t);
}
