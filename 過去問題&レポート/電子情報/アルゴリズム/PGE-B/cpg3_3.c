#include<stdio.h>

void str_rev(char s[],int k){
  int i,n;
  for(i=0;i<k/2;i++){
    n=s[i];
    s[i]=s[k-i-1];
    s[k-i-1]=n;
  }
}

int base_transf(int m,int p,char str[]){
  char dc[]="0123456789ABCDEF";
  int k=0;
  if(m==0){
    str[0]=dc[0];
    k=1;
  }else{
    while(m){
      str[k]=dc[m%p];
      m/=p;
      k++;
    }
  }
  str_rev(str,k);
  return(k);
}

main(){
  int i,k;
  char str[100];
  k=base_transf(250,16,str);
  for(i=0;i<k;i++) printf("%c",str[i]);
}
