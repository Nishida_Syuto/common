#include<stdio.h>
#include<stdlib.h>

unsigned int bits(unsigned int x,int k,int j){
  return (x>>k)&~(~0<<j);
}

void radix_exchange(int d[],int pass,int p,int q){
  int i,j,t;

  if(p<q && pass>=0){
    i=p;
    j=q;
    while(i!=j){
      while(bits(d[i],pass,1)==0 && i<j)i++;
      while(bits(d[j],pass,1)!=0 && i<j)j--;
      t=d[i];d[i]=d[j];d[j]=t;
    }
    if(bits(d[q],pass,1)==0)j++;
    radix_exchange(d,pass-1,p,j-1);
    radix_exchange(d,pass-1,j,q);
  }
}


int main(int argc,char *argv[]){
  FILE *fp;
  int *d,n,i;

  n=atoi(argv[1]);
  d=(int *)malloc(sizeof(int)*(n+1));
  d[0]=-1;

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=n;i++)
    fscanf(fp,"%d\n",&d[i]);
  fclose(fp);

  radix_exchange(d,30,1,n);
  for(i=1;i<=n;i++)
    printf("%d\n",d[i]);
  return 0;
}
