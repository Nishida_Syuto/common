#include<stdio.h>

void hanoi(int n,int x,int y){
  if(n>1)hanoi(n-1,x,6-x-y);
  printf("Move disk[%d] from axis[%d] to axis[%d].\n",n,x,y);
  if(n>1)hanoi(n-1,6-x-y,y);
}

int main(void){
  int n,start,goal;

  scanf("%d %d %d",&n,&start,&goal);
  printf("Tower of Hanoi");
  printf("(How to move %d disks from axis[%d] to axis[%d])\n",n,start,goal);
  hanoi(n,start,goal);
  return(0);
}
