#include<stdio.h>
#include<stdlib.h>

int *b;

void merge_sort(int *d,int p,int q){
  int i,j,k,m;

  if(p<q){
    m=(p+q)/2;
    merge_sort(d,p,m);
    merge_sort(d,m+1,q);
    for(i=m+1;i>p;i--)b[i-1]=d[i-1];
    for(j=m;j<q;j++)b[q+m-j]=d[j+1];
    for(k=p;k<=q;k++){
      d[k]=(b[i]<b[j])?b[i++]:b[j--];
    }
  }
}

int main(int argc,char *argv[]){
  FILE *fp;
  int *a,n,i;

  n=atoi(argv[1]);
  a=(int *)malloc(sizeof(int)*(n+1));
  a[0]=-1;
  b=(int *)malloc(sizeof(int)*(n+1));

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=n;i++){
    fscanf(fp,"%d\n",&a[i]);
  }
  fclose(fp);

  merge_sort(a,1,n);
  for(i=1;i<=n;i++){
    printf("%d\n",a[i]);
  }
}
