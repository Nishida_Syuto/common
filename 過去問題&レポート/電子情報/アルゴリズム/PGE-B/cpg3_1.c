#include<stdio.h>
#include<math.h>

double inprod(double *x,double *y,int n){
  int i;
  double w=0.0;
  for(i=0;i<n;i++) w+=x[i]*y[i];
  return w;
}

double norm(double *x,int n){
  double w;
  w=sqrt(inprod(x,x,n));
  return w;
}

main(){
  double u[4],v[4];
  int i;
  for(i=0;i<4;i++) scanf("%lf",&u[i]);
  for(i=0;i<4;i++) scanf("%lf",&v[i]);
  printf("%f \n",inprod(u,v,4));
  printf("%f \n",norm(u,4));
}
