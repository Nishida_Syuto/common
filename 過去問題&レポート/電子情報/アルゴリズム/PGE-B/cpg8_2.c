#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
  char country[20];
  char city[20];
  struct node *next;
}LISTEL;

struct node *insert(char *x,char *y,struct node *p,struct node *root){
  struct node *q,*q0;

  if(root==NULL && p!=NULL){
    printf("Input error.\n");
    exit(1);
  }

  q0=(struct node *)malloc(sizeof(LISTEL));

  if(p==NULL){
    q=root;
    root=q0;
  }else{
    q=p->next;
    p->next=q0;
  }

  strcpy(q0->country,x);
  strcpy(q0->city,y);
  q0->next=q;
  return(root);
}

struct node *delete(struct node *p,struct node *root){
  struct node *q;

  if(p==NULL || root==NULL){
    printf("Error.\n");
    exit(1);
  }

  if(p==root){
    q=root->next;
    free(root);
    root=q;
  }else{
    q=root;
    while(q->next!=p)q=q->next;
    q->next=p->next;
    free(p);
  }
  return(root);
}

void write_list(struct node *p){
  while(p!=NULL){
    printf("%s %s\n",p->country,p->city);
    p=p->next;
  }
  printf("\n");
}

int main(void){
  struct node *root,*q;
  char country[20],city[20];

  root=NULL;

  q=NULL;
  strcpy(country,"Japan");
  strcpy(city,"Tokyo");
  root=insert(country,city,q,root);

  q=root;
  strcpy(country,"America");
  strcpy(city,"Washington");
  root=insert(country,city,q,root);

  q=NULL;
  strcpy(country,"Russia");
  strcpy(city,"Moscow");
  root=insert(country,city,q,root);

  q=root;
  strcpy(country,"China");
  strcpy(city,"Beijing");
  root=insert(country,city,q,root);

  write_list(root);

  q=root->next;
  root=delete(q,root);

  write_list(root);

  return 0;
}
