#include<stdio.h>
#define STACK_SIZE 2

int stack[STACK_SIZE];
int pos;

void init_stack(void){
  printf("Stack size:%d\n",STACK_SIZE);
  pos=-1;
}

void push(int data){
  if(pos<STACK_SIZE-1)
    stack[++pos]=data;
  else
    printf("Error:Stack full!\n");
}

int pop(void){
  if(pos>=0)
    return stack[pos--];
  else
    printf("Error:Stack empty!\n");
}

void print_stack(void){
  int i;
  printf("Stack:");
  for(i=pos;i>=0;i--){
    printf("%d",stack[i]);
  }
  printf("\n");
}

int main(void){
  init_stack();
  pop();
  push(4);print_stack();
  push(3);print_stack();
  pop();print_stack();
  push(2);print_stack();
  push(1);print_stack();
  return(0);
}
