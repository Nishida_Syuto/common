#include<stdio.h>
#include<stdlib.h>

void quick_sort(int *d,int left,int right){
  int i,j,p,t;
  p=d[(left+right)/2];
  i=left;j=right;
  while(i<=j){
    while(d[i]<p)i++;
    while(d[j]>p)j--;
    if(i<=j){
      t=d[i];d[i]=d[j];d[j]=t;
      i++;j--;
    }
  }
  if(left<j)quick_sort(d,left,j);
  if(i<right)quick_sort(d,i,right);
}

int main(int argc,char *argv[]){
  FILE *fp;
  int *data,num_of_data,i;

  num_of_data=atoi(argv[1]);
  data=(int *)malloc(sizeof(int)*(num_of_data+1));
  data[0]=-1;

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=num_of_data;i++){
    fscanf(fp,"%d\n",&data[i]);
  }
  fclose(fp);

  quick_sort(data,1,num_of_data);
  for(i=1;i<=num_of_data;i++)printf("%d\n",data[i]);
  return 0;
}
