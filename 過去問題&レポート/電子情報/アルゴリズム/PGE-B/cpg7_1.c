#include<stdio.h>
#include<stdlib.h>

typedef struct node{
  int data;
  struct node *next;
}LISTEL;

struct node *root;

void construct_list(){
  struct node *p;
  int i,y;
  for(i=0;i<5;i++){
    scanf("%d",&y);
    p=(struct node *)malloc(sizeof(LISTEL));
    p->data=y;
    p->next=root;
    root=p;
  }
}

void write_list(){
  struct node *p;
  p=root;
  while(p!=NULL){
    printf(" %d",p->data);
    p=p->next;
  }
  printf("\n");
}

main(){
  root=NULL;
  construct_list();
  write_list();
}
