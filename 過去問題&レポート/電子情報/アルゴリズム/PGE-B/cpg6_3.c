#include<stdio.h>
#define SIZE 8

int queue[SIZE];
int head,tail,n;

void init_queue(void){
  head=0;
  tail=0;
  n=0;
}

void prt_queue(void){
  int i,curr=tail;

  printf("Queue:");
  for(i=0;i<n;i++){
    printf("q[%d]:%d ",curr,queue[curr]);
    if(--curr==-1)curr+=SIZE;
  }
  printf("\n\thead:%d tail:%d n:%d\n",head,tail,n);
}

void enter(int x){
  printf("Enqueue %d,",x);
  if(n<SIZE){
    tail++;
    if(tail==SIZE)tail=0;
    n++;
    queue[tail]=x;
  }else printf("Queue full error!\n");
  prt_queue();
}

int remov(void){
  printf("Dequeue,");
  if(n>0){
    head++;
    if(head==SIZE)head=0;
    n--;
    prt_queue();
    return queue[head];
  }else{
    printf("Queue enpty error!\n");
    return -1;
  }
}

int main(void){
  int i;

  init_queue();

  printf("Queue size:%d\n",SIZE);

  remov();

  for(i=0;i<SIZE;i++){
    enter(i+1);
  }

  enter(9);

  remov();
  remov();

  enter(10);
  return 0;
}
