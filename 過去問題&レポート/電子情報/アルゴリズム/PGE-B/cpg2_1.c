#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(int argc,char *argv[]){
  FILE *fp1,*fp2;
  char *fn1,*fn2;
  int i,x;
  double m,n;

  if(argc==3){
    fn1=argv[1];
    fn2=argv[2];
  }else{
    printf("%s file1 file2\n",argv[0]);
    exit(1);
  }

  if((fp1=fopen(fn1,"w"))==NULL){
    printf("Can't open %s.\n",fn1);
    exit(1);
  }

  if((fp2=fopen(fn2,"w"))==NULL){
    printf("Can't open %s.\n",fn2);
    exit(1);
  }

  for(i=0;i<100;i++){
    x=rand();
    fprintf(fp1,"%d\n",x);
  }

  m=0.1;
  while(m<=50){
    n=(100-pow(m-8,2))*sin(2*m+1);
    fprintf(fp2,"%f\n",n);
    m+=0.1;
    /*printf("%0.15f\n",m);*/
}

  fclose(fp1);
  fclose(fp2);
  return 0;
}
