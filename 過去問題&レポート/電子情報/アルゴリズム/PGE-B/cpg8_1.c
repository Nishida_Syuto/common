#include<stdio.h>
#include<stdlib.h>

typedef struct node{
  int data;
  struct node *next;
}LISTEL;

struct node *insert(int x,struct node *root){
  struct node *p;

  p=(struct node *)malloc(sizeof(LISTEL));

  p->data=x;
  p->next=root;

  return(p);
}

void write_list(struct node *p){
  while(p!=NULL){
    printf("%d\n",p->data);
    p=p->next;
  }
  printf("\n");
}

int main(void){
  struct node *root1=NULL;
  struct node *root2=NULL;
  int x;

  x=1;
  root1=insert(x,root1);

  x=2;
  root1=insert(x,root1);

  x=3;
  root2=insert(x,root2);

  x=4;
  root2=insert(x,root2);

  write_list(root1);
  write_list(root2);

  return(0);
}
