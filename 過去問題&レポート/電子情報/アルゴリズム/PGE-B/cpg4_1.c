#include<stdio.h>

int gcd_euclidean1(int m,int n){
  int r;

  while(n!=0){
    r=m%n;
    m=n;
    n=r;
  }
  return m;
}

int gcd_euclidean2(int m,int n){
  if(m%n==0) return n;
  return gcd_euclidean2(n,m%n);
}

int main(void){
  int x,y,t;

  scanf("%d %d",&x,&y);

  if(x<y){
    t=x;x=y;y=t;
  }

  printf("%d-%d:GCD1=%d\n",x,y,gcd_euclidean1(x,y));
  printf("%d-%d:GCD2=%d\n",x,y,gcd_euclidean2(x,y));
  return(0);
}
