#include<stdio.h>
#include<stdlib.h>

typedef struct node{
  int data;
  struct node *next;
}LISTEL;

struct node *root;

void construct_list(){
  struct node *p;
  int i,y;
  for(i=0;i<5;i++){
    scanf("%d",&y);
    p=(struct node *)malloc(sizeof(LISTEL));
    p->data=y;
    p->next=root;
    root=p;
  }
}

void write_list(){
  struct node *p;
  p=root;
  while(p!=NULL){
    printf(" %d",p->data);
    p=p->next;
  }
  printf("\n");
}

struct node *search(int x){
  struct node *p;
  p=root;
  while(p!=NULL){
    if(p->data==x){
      printf("\t%d found.\n",x);
      return(p);
    }
    else
      p=p->next;
  }
  printf("\t%d not found.\n",x);
  return(NULL);
}

main(){
  int x;
  root=NULL;
  construct_list();
  write_list();
  scanf("%d",&x);
  search(x);
}
