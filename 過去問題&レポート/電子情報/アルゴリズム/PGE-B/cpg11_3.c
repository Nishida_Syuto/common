#include<stdio.h>
#include<stdlib.h>

void downheap(int *d,int j,int k){
  int i,v;

  v=d[j];
  while(j<=k/2){
    i=2*j;
    if(i<k && d[i]<d[i+1])i++;
    if(v>=d[i])break;
    d[j]=d[i];
    j=i;
  }
  d[j]=v;
}

void heap_sort(int *d,int n){
  int j,k,t;

  for(j=n/2;j>=1;j--)downheap(d,j,n);
  k=n;
  while(k>1){
    t=d[1];d[1]=d[k];d[k]=t;
    downheap(d,1,--k);
  }
}

int main(int argc,char *argv[]){
  FILE *fp;
  int *a,n,i;

  n=atoi(argv[1]);
  a=(int *)malloc(sizeof(int)*(n+1));
  a[0]=-1;

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=n;i++)
    fscanf(fp,"%d\n",&a[i]);
  fclose(fp);

  heap_sort(a,n);
  for(i=1;i<=n;i++){
    printf("%d\n",a[i]);
  }
}
