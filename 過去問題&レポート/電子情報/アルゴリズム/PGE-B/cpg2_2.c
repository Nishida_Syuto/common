#include<stdio.h>
#include<stdlib.h>
#define MAXDATA 1000

int main(int argc,char *argv[]){
  FILE *fp1,*fp2;
  char *fn1,*fn2;
  int n,m,i;
  int rand_int[MAXDATA],x;
  double *rand_real,y;

  if(argc==3){
    fn1=argv[1];
    fn2=argv[2];
  }else{
    printf("%s file1 file2\n",argv[0]);
    exit(1);
  }

  if((fp1=fopen(fn1,"r"))==NULL){
    printf("Can't open %s.\n",fn1);
    exit(1);
  }

  if((fp2=fopen(fn2,"r"))==NULL){
    printf("Can't open %s.\n",fn2);
    exit(1);
  }

  n=0;
  while(fscanf(fp1,"%d",&x)!=EOF){
    rand_int[n]=x;
    n++;
  }
  printf("%s:%d\n",argv[1],n);

  m=0;
  while(fscanf(fp2,"%lf",&y)!=EOF)
    m++;
  
  rand_real=(double*)malloc(m*sizeof(double));
  
  for(i=0;i<m;i++){
    fscanf(fp2,"%lf",&y);
    rand_real[i]=y;
  }
  printf("%s:%d\n",argv[2],m);


  fclose(fp1);
  fclose(fp2);
  return 0;
}
