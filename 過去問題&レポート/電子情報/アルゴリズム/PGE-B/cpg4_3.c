#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double randdf(double a,double b){
  double r,r1,r2;
  r1=(double)rand();
  r2=(double)RAND_MAX;
  r=(r1/r2)*(b-a)+a;
  return(r);
}

double monte_carlo(double (*f)(double),double a,double b,double c,int n){
  double x,y;
  int count=0,i;

  for(i=0;i<n;i++){
    x=randdf(a,b);
    y=randdf(0,c);
    if((*f)(x)>y)count++;
  }
  return((double)count/(double)n*(b-a)*c);
}

double h(double x){
  double y;
  y=sqrt(1.0-x*x);
  return(y);
}

int main(void){
  double s;
  s=monte_carlo(h,-1.0,1.0,1.0,1000000);
  printf("%f\n",s);
  return(0);
}
