#include<stdio.h>
#include<stdlib.h>

int *st;
int sp;
int stack_size;

void init_stack(int n){
  st=(int *)malloc(sizeof(int)*n);
  sp=-1;
}

void push(int e){
  if(sp<stack_size-1)
    st[++sp]=e;
    else{
      printf("stack full error!\n");
      exit (1);
    }
}

int pop(){
  if(sp>=0)
    return st[sp--];
    else{
      printf("stack empty error!\n");
      exit (1);
    }
}

int empty_stack(){
  return(sp==-1);
}

void quick_sort2(int *d,int n){
  int left,right,i,j,p,t;

  init_stack(n);
  push(1);
  push(n);

  while(!empty_stack()){
    right=pop();
    left=pop();
    p=d[(left+right)/2];
  i=left;
  j=right;
  while(i<=j){
    while(d[i]<p)i++;
    while(d[j]>p)j--;
    if(i<=j){
      t=d[i];d[i]=d[j];d[j]=t;
      i++;j--;
    }
  }
  if(left<j){
    push(left);
    push(j);
  }
  if(i<right){
    push(i);
    push(right);
  }
  }
}


int main(int argc,char *argv[]){
  FILE *fp;
  int *a,n,i;

  n=atoi(argv[1]);
  a=(int *)malloc(sizeof(int)*(n+1));
  a[0]=-1;
  stack_size=n;

  if((fp=fopen(argv[2],"r"))==NULL){
    printf("Can't open %s.\n",argv[2]);
    exit(1);
  }

  for(i=1;i<=n;i++){
    fscanf(fp,"%d\n",&a[i]);
  }
  fclose(fp);

  quick_sort2(a,n);
  for(i=1;i<=n;i++){
    printf("%d\n",a[i]);
  }
}
