#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
  char univ[20];
  char state[20];
  struct node *next;
}LISTEL;

struct node *root;

void construct_list(char *fn){
  struct node *p;
  int i;
  char y[20],z[20];
  FILE *fp;

  if((fp=fopen(fn,"r"))==NULL){
    printf("Can't open %s.\n",fn);
    exit(1);
  }

  for(i=0;i<8;i++){
    fscanf(fp,"%s %s",y,z);
    p=(struct node *)malloc(sizeof(LISTEL));
    strcpy(p->univ,y);
    strcpy(p->state,z);
    p->next=root;
    root=p;
  }
  fclose(fp);
}

void write_list(){
  struct node *p;
  p=root;
  printf("\n");
  while(p!=NULL){
    printf("%s %s\n",p->univ,p->state);
    p=p->next;
  }
  printf("\n");
}

struct node *search(char *x){
  struct node *p;
  int cwt=0;
  p=root;
  while(p!=NULL){
    cwt++;
    if(strcmp(p->univ,x)==0){
      printf("%d %s\n\n",cwt,p->state);
      return(p);
    }else
      p=p->next;
  }
  printf("%s not found.\n\n",x);
  return(NULL);
}

void add_node(struct node *q){
  struct node *p;
  int i;
  char y[20],z[20];
  
  scanf("%s %s",y,z);
  p=(struct node *)malloc(sizeof(LISTEL));
  strcpy(p->univ,y);
  strcpy(p->state,z);
  
  if(q!=NULL){
    p->next=q->next;
    q->next=p;
  }else{
    p->next=root;
    root=p;
  }
}

main(int argc,char **argv){
  struct node *q;
  char x[20];
  root=NULL;
  construct_list(argv[1]);
  write_list();
  scanf("%s",x);
  q=search(x);
  add_node(q);
  write_list();

}
