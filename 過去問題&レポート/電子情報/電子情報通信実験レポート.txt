3.レポート課題3
LED-1)

int led = 13;

void setup() {                
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);  

  delay(2000);

  digitalWrite(led, LOW);

  delay(2000);

}



LED-2)

int led = 13;

void setup() {                
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);

  delay(500);

  digitalWrite(led, LOW);

  delay(300);

}



LED-4)

int led = 13;

void setup() {                
  pinMode(led, OUTPUT);

  pinMode(12,OUTPUT);

}

void loop() {
  digitalWrite(led, HIGH);

  digitalWrite(12,HIGH);

  delay(500);
  digitalWrite(led, LOW);

  digitalWrite(12,LOW);

  delay(500);
}



LED-5)

int led = 13;

void setup() {                
  pinMode(led, OUTPUT);
  pinMode(12,OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);

  digitalWrite(12,LOW);

  delay(500);
  digitalWrite(led, LOW);

  digitalWrite(12,HIGH);

  delay(500);
}



4.レポート課題4
tone-1)

 #include "pitches.h"

int melody[] = {

  NOTE_C4,NOTE_D4,NOTE_E4,NOTE_F4,NOTE_G4,NOTE_A4,NOTE_B4,NOTE_C5};

int noteDurations[] = {

  4,4,4,4,4,4,4,4 };

void setup() {
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000/noteDurations[thisNote];
    tone(8, melody[thisNote],noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(8);
  }
}



tone-2)

 #include "pitches.h"

int melody[] = {
  NOTE_C4,NOTE_D4,NOTE_E4,NOTE_F4,NOTE_G4,NOTE_A4,NOTE_B4,NOTE_C5};

int noteDurations[] = {
  4,4,4,4,4,4,4,4 };

void setup() {  
  pinMode(13,OUTPUT);
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000/noteDurations[thisNote];

    digitalWrite(13,HIGH);

    tone(8, melody[thisNote],noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;

    digitalWrite(13,LOW);

    delay(pauseBetweenNotes);
    noTone(8);
  }
}