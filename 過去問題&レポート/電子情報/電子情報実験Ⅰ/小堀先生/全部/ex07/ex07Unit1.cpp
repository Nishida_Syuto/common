//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ex07Unit1.h"
#include "caio.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	long Ret;

	Ret = AioInit("AIO000", &Id);
	if(Ret != 0){
	ShowMessage("初期化処理エラー");
	return;
	}

	Ret = AioSingleAoEx(Id, 0, 5.0);

	ShowMessage("初期化処理 : 正常終了");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	long Ret;

	Ret = AioExit(Id);
	if(Ret != 0){
		ShowMessage("終了処理エラー");
		return;
	}
	ShowMessage("終了処理 : 正常終了");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Timer1->Enabled = True;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Timer1->Enabled = False;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	long Ret;
	float AiData,ave,temp,sum=0;
	int i; //sum=合計,ave=平均,temp=温度

	for(i=1;i<=100;i++){
		Ret=AioSingleAiEx(Id, 0, &AiData);
		sum=sum+AiData;
	}

	ave=sum/100;
	temp=ave*100;

	Label2->Caption = FormatFloat("0.0;-0.0", temp);
}
//---------------------------------------------------------------------------
