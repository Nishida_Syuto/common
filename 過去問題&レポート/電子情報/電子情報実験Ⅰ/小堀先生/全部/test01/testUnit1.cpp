//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "testUnit1.h"
#include "caio.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	long Ret;

	Ret = AioInit("AIO000", &Id);
	if(Ret != 0){
		ShowMessage("初期化処理エラー");
		return;
		}
	Ret = AioSingleAoEx(Id, 0, 5.0);

	ShowMessage("初期化処理 : 正常終了");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	long Ret;

	Ret = AioExit(Id);
	if(Ret !=0){
		ShowMessage("終了処理エラー");
		return;
		}
	ShowMessage("終了処理 : 正常終了");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Timer1->Enabled =True;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Timer1->Enabled=False;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	long Ret;
	float AiData,sum=0,ave,temp,Upper,Lower,Power,PowerSum;
	int i;

	for(i=1;i<=100;i++){
		Ret = AioSingleAiEx(Id, 0, &AiData);
		sum=sum+AiData;
	}
	ave=sum/100;
	temp=ave*100;

	Label2->Caption = FormatFloat("0.0;-0.0", temp);

	//温度誤差
	Upper=StrToInt(Edit1->Text);
	Lower=StrToInt(Edit2->Text);
	if(Upper<temp){
	Label5->Caption ="上限値+"+FormatFloat("0.0",temp-Upper);
	}else if(Lower<=temp) {
	Label5->Caption ="正常範囲";
	}else if(temp<Lower){
	Label5->Caption ="上限値-"+FormatFloat("0.0",Lower-temp);

	}
}
//---------------------------------------------------------------------------


