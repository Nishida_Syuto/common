//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ex03Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Label1->Caption="Welcome to C++Builder";
	Label2->Caption="Welcome to C++Builder";
	Label3->Caption="Welcome to C++Builder";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Label1->Caption="";
	Label2->Caption="";
	Label3->Caption="";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Label1->Caption="";
	Label2->Caption="";
	Label3->Caption="";
}
//---------------------------------------------------------------------------
