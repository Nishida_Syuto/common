#include<stdio.h>
#define EPS (1.0e-10)

int main(int argc,char *argv[]){
  
  int i;
  double xi,xinext,a,fx;
  sscanf(argv[1],"%lf",&a);
  i=0,xi=5;
  xi=a;
  
  do{
    xinext=(xi+a/xi)/2;
    fx=xinext*xinext-a;
    printf("i+1=%d xinext=%e fx=%e\n",i+1,xinext,fx);
    i++;
    xi=xinext;
  }
  while(fx<= -EPS || fx >= EPS);
  return(0);
  
}
