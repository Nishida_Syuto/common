#include<stdio.h>
#define EPS (1.0e-10)
#define ITMAX (1000)
int main(int argc,char *argv[]){
  double fx;
  int i;
  double xi,xinext,a;
  double eps,x0;
  int k,itmax;
  sscanf(argv[argc-1],"%lf",&a);
  x0=a;
  eps = EPS;
  itmax = ITMAX;
  k=1;
  
  while(k<argc-1){   
    if(strcmp(argv[k],"-E")==0){
      k++; 
      sscanf(argv[k],"%lf",&eps);
    }
    else if(strcmp(argv[k],"-I")==0){
      k++;
      sscanf(argv[k],"%lf",&x0);
    }
    else if(strcmp(argv[k],"-L")==0){
      k++;
      sscanf(argv[k],"%d",&itmax);
    }
    else{return(255);}
    k++;
  }

  printf("収束判定基準値=%e 初期値=%e 反復回数の上限値%d\n",eps,x0,itmax);
  xi=x0;
  i=0;
  do{
    xinext=(xi+a/xi)/2;
    fx=xinext*xinext-x0;
    printf("i+1=%d xinext=%e fx=%e\n",i+1,xinext,fx);
    i++;
    xi=xinext;
  }
  while((fx<= -eps || fx >= eps) && i<=itmax);
  return(0);
  
}
