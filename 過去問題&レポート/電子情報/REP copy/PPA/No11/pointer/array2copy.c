#include<stdio.h>
#define IMAX (3)
#define JMAX (4)

main(){
  int c[IMAX][JMAX]={
    {0,1,2,3},
    {4,5,6,7},
    {8,9,10,11}
    };
  int *p;
  double d[IMAX][JMAX];
  double *q;
  int i,j,k;
  for(p=(int *)c,q=(double*)d,k=0;k<IMAX*JMAX;k++,p++,q++){
    *q=(double)(*p);
  }
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("c[%d][%d]=%d &c[%d][%d]=%p\n",i,j,c[i][j],i,j,&c[i][j]);
  }
  }
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("d[%d][%d]=%g &d[%d][%d]=%p\n",i,j,d[i][j],i,j,&d[i][j]);
  }
  }
}
