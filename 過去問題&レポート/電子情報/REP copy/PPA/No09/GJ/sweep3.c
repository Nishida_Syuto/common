#include<stdio.h>
#define IMAX 4
#define JMAX 5

double array[IMAX][JMAX];
double arrayA[IMAX][JMAX],arrayB[IMAX];
double arrayC[IMAX],arrayS[IMAX],arrayAS[IMAX];


void PrintMat(void){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("%5.2f",array[i][j]);
    }
    putchar('\n');
  }
  putchar('\n');
}

void SweepOut(void){
  int i,j,axis;
  double pivot,aik;
  
  for(axis=0;axis<IMAX;axis++){
    pivot=array[axis][axis];
    for(j=axis;j<JMAX;j++){
      array[axis][j]/=pivot;
    }
    for(i=0;i<IMAX;i++){
    if(i!=axis){
      aik=array[i][axis];
      for(j=0;j<JMAX;j++){
	array[i][j]-=array[axis][j]*aik;
      }
    }
    }
  }
}
main(){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      scanf("%lf",&array[i][j]);
    }
  }
  
  for(i=0;i<IMAX;i++){
    arrayB[i]=array[i][JMAX-1];
    for(j=0;j<IMAX;j++){
      arrayA[i][j]=array[i][j];
    }
  }
  
  PrintMat();
  SweepOut();
  PrintMat();
  
  for(i=0;i<IMAX;i++){
     arrayS[i]=array[i][JMAX-1];
  }

  for(i=0;i<IMAX;i++){
    for(j=0;j<IMAX;j++){
      arrayAS[i]+=arrayA[i][j]*arrayS[j];
    }
  }
  
  for(i=0;i<IMAX;i++){
    arrayC[i]=arrayAS[i]-arrayB[i];
  }

  for(i=0;i<IMAX;i++){
    printf("%g\n",arrayC[i]);
  }
}
