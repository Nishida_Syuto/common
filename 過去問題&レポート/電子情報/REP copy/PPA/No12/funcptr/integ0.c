#include<stdio.h>

double func(double x);
double integra10(double (*func)(),double a,double b,int n);

main(){
  double a,b;
  int n;
  printf("積分区間[a,b]と区関数n a,b,n:");
  scanf("%lf,%lf,%d",&a,&b,&n);
  printf("長方形近似 I=%20.17f\n",integra10(func,a,b,n));
}

double func(double x){
  return(2.0/(1.0+x*x));
}

double integra10(double (*func)(),double a,double b,int n){
  double h=(b-a)/n,
    sum=0.0,x,answer;
  int i;
  for(i=0,x=a;i<n;i++,x+=h){
    sum+=func(x);
  }
  answer=h*sum;
  return(answer);
}
 
