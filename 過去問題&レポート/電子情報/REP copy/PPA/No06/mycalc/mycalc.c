#include<stdio.h>
#include<string.h>
#include<math.h>

main(int argc,char *argv[]){
  double x,y;
  char *fname;
  int n;
  
  if(argc < 3){
    Return(-1);
  }
  if(argc  > 3){
    return(-2);
  }
  n =sscanf(argv[2],"%lf",&x);
  if(n !=1){
    return(-4);
  }

  fname = argv[1];
  if(strcmp(fname, "sqrt") == 0){
    if(0 <= x){
      y = sqrt(x);
	}
    else{return(-4);}
  }
  else if(strcmp(fname,"exp") == 0){
    y = exp(x);
  }
  else if(strcmp(fname,"cos") == 0){
    y = cos(x);
  }
  else if(strcmp(fname,"acos") == 0){
    if(-1<=x && 1>=x){
      y = acos(x);
    }
    else{return(-4);}
  }
  else if(strcmp(fname,"fabs") == 0){
    y = fabs(x);
  }
  else{
    return(-3);
  }
  printf("%e\n",y);
  return(0);
}
