#include<stdio.h>
#define EPS (1.0e-10)

int main(){

  int i;
  double xi,xinext,a,dxi;
  
  a=1,i=0,xi=5;
  
  do{
    xinext=(xi+a/xi)/2;
    dxi=xinext-xi;
    printf("i=%d xinext=%e xi=%e dxi=%e\n",i,xinext,xi,dxi);
    i++;
    xi=xinext;
  }
  while(dxi<= -EPS || dxi >= EPS);
  return(0);
  
}
