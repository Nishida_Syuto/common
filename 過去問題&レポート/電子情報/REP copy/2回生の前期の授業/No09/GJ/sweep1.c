

#include<stdio.h>
#define IMAX 3
#define JMAX 4

double array[IMAX][JMAX];


void PrintMat(void){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("%5.2f",array[i][j]);
    }
    putchar('\n');
  }
  putchar('\n');
}

void SweepOut(void){
  int i,j,axis;
  double pivot,aik;
  
  for(axis=0;axis<IMAX;axis++){
    pivot=array[axis][axis];
    for(j=axis;j<JMAX;j++){
      array[axis][j]/=pivot;
    }
    for(i=0;i<IMAX;i++){
    if(i!=axis){
      aik=array[i][axis];
      for(j=0;j<JMAX;j++){
	array[i][j]-=array[axis][j]*aik;
      }
    }
    }
  }
}
main(){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      scanf("%lf",&array[i][j]);
    }
  }

  PrintMat();
  SweepOut();
  PrintMat();
}
