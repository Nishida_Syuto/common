#include<stdio.h>
double fact1(int n);
double fact2(int n);

main(){
  int a;
  printf("a=");
  scanf("%d",&a);
  printf("%d!=%f(def.1)\n",a,fact1(a));
  printf("%d!=%f(def.2)\n",a,fact2(a));
  return(0);
}

double fact1(int n){
  int i;
  double ans=1.0;
  for(i=1;i<=n;i++){
    ans*=i;
  }
  return(ans);
}
double fact2(int n){
  if(n==0){
    return(1);
  }else{
    return(n*fact2(n-1));
  }
}
  
