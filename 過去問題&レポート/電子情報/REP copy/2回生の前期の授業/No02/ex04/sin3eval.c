#include <stdio.h>
#include <math.h>

double sin3(double x);


  int main(){
    double x;
    for(x=0;x<=2*M_PI;x+=(M_PI/8)){
      printf("x%f\n",x);

      printf("sin%f=%f(sin3)\n",x,sin3(x));
      printf("sin%f=%f(math's sin)\n",x,sin3(x));
    }
    return(0);

  }
