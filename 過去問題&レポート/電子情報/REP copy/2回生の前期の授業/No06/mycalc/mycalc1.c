#include<stdio.h>
#include<string.h>
#include<math.h>

main(int argc,char *argv[]){
  
  //初期化
  double x,y;
  char *fname;
  int n;
  
  
  if(argc < 3){
    return(-1);
  }
  n =sscanf(argv[2],"%lf",&x);
  if(n !=1){
    return(-2);
  }

  //選択構造
  fname = argv[1];
  if(strcmp(fname, "sqrt") == 0){
    y = sqrt(x);
  }
  else if(strcmp(fname,"exp") == 0){
    y = exp(x);
  }
  else if(strcmp(fname,"cos") == 0){
    y = cos(x);
  }
  else if(strcmp(fname,"acos") == 0){
    y = acos(x);
  }
  else if(strcmp(fname,"fabs") == 0){
    y = fabs(x);
  }
  else{
    return(-3);
  }
  //最終段階
  printf("%e\n",y);
  return(0);
   
    }
