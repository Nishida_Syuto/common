#include<stdio.h>
#include<string.h>
#include<math.h>

main(int argc,char *argv[]){
  double x,y;
  char *fname;
  int n;
  
  if(argc < 3){
    return(-1);
  }
  n =sscanf(argv[2],"%lf",&x);
  if(n !=1){
    return(-2);
  }

  fname = argv[1];
  if(strcmp(fname, "sqrt") == 0){
    y = sqrt(x);
  }
  else{
    return(-3);
  }

  printf("%10g\n",y);
  return(0);
   
    }
