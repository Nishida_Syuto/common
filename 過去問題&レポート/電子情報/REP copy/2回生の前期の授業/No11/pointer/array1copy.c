#include<stdio.h>
#define N (8)

main(){
  
  int a[N]={10,11,12,13,14,15,16,17};
  int *p;
  double b[N];
  double *q;
  int k;
  
  for(p=(int*)a,q=(double*)b,k=0;k<N;k++,p++,q++){
    *q = (double)(*p);
  }
  for(k=0;k<N;k++){
    printf("a[%d]=%d &a[%d]=%p\n",k,a[k],k,&a[k]);
  }
  for(k=0;k<N;k++){
    printf("b[%d]=%f &b[%d]=%p\n",k,b[k],k,&b[k]);
  }
}
