
#include<stdio.h>
#include<math.h>

double func(double x);
double integral1(double (*func)(),double a,double b,int n);

main(){
  double a,b;
  int n;
  printf("積分区間[a,b]と区関数n a,b,n :");
  scanf("%lf,%lf,%d",&a,&b,&n);
  printf("台形近似　I=%20.17f\n",integral1(func,a,b,n));
  printf("誤差　%f\n",M_PI-integral1(func,a,b,n));
}

double func(double x){
    return(4.0/(1.0+x*x));
  }

double integral1(double (*func)(),double a,double b,int n){
  double h=(b-a)/n,
    sum=0.0,x,answer;
  int i;
  
  for(i=0,x=a;i<n;i++,x+=h){
    sum+=(func(x)+(func(x+h))/2)*h;
  }
  answer=sum;
  return(answer);
}

         
