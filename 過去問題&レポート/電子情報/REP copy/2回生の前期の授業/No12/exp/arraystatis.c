#include<stdio.h>
#include<math.h>
#define N (256)

int statis(double *array,int n,double *ave,double *sigma);

main(){
  int i;
  double array,ave,sigma,a[N];
  
  for(i=0;i<N;i++){a[i]=i+1;}
  statis(a,N,&ave,&sigma);
  printf("平均＝%f,標準偏差＝%f\n",ave,sigma);
}



int statis(double *array,int n,double *ave,double *sigma){
  double m;
  int i;
  
  

  for(i=0,m=0;i<n;i++){
    m+=array[i]/n;
  }
  
  *ave=m;
  
  for(i=0, *sigma=0;i<n;i++){
    *sigma+=((array[i]-m)*(array[i]-m))/(n-1);
    
  }
  *sigma=sqrt(*sigma);
  return(0);
}

