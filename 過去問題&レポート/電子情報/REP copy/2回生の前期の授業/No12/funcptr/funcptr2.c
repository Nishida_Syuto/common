#include<stdio.h>
#include<math.h>
#define PI (3.14159265)

double add2funcs(double (*fa)(),double (*fb)(),double value);

main(){
  double a; 
  a=add2funcs(sin,cos,PI/4);
  printf("%f must be equal to 1.\n",a);
    }

double add2funcs(double (*fa)(),double (*fb)(),double value){
  double a,b;
  a=fa(value);
  b=fb(value);
  return(a*a+b*b);
}
