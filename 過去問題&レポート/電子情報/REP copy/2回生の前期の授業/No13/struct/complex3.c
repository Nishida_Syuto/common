
#include<stdio.h>
#define PrintComplex(x) \
  printf("(%f%s%fi)",(x).real,((x).img>=0)?"+":"",(x).img)

struct COMPLEX{double real;double img;};
struct COMPLEX AddComplex(struct COMPLEX *c,struct COMPLEX *a,struct COMPLEX *b);

main(){
  struct COMPLEX a={1,3},b={5,3},c;
  AddComplex(&c,&a,&b);
  PrintComplex(a);putchar('+');
  PrintComplex(b);putchar('=');
  PrintComplex(c);putchar('\n');
}


struct COMPLEX AddComplex(struct COMPLEX *c,struct COMPLEX *a,struct COMPLEX *b){
  c->real = a->real + b->real;
  c->img = a->img + b->img;
}
