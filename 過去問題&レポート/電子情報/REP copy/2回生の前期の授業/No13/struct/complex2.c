
#include<stdio.h>
#define PrintComplex(x) \
  printf("(%f%s%fi)",(x).real,((x).img>=0)?"+":"",(x).img)

struct COMPLEX{double real;double img;};
struct COMPLEX AddComplex(struct COMPLEX a,struct COMPLEX b);

main(){
  struct COMPLEX a={1,3},b={5,3},c;
  c=AddComplex(a,b);
  PrintComplex(a);putchar('+');
  PrintComplex(b);putchar('=');
  PrintComplex(c);putchar('\n');
}

struct COMPLEX AddComplex(struct COMPLEX a,struct COMPLEX b){
  struct COMPLEX ans;
  ans.real=a.real+b.real;
  ans.img=a.img+b.img;
  return(ans);
}


