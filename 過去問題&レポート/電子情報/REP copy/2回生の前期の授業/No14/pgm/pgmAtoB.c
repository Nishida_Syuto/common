
#include<stdio.h>
#include<stdlib.h>

#define H (24)
#define W (32)

int main(){
  FILE *ifp;
  FILE *ofp;
  char infile[]="imageA32.pgm";
  char outfile[]="imageB32.pgm";
  unsigned char image[H][W];
  unsigned char *chp, ch_data,ch_max;
  int k, int_data,i,j;
  
  ifp=fopen(infile,"r");
  ofp=fopen(outfile,"w");
  if(ifp==NULL){
    fprintf(stderr,"fopen error %s\n",infile);
    exit(-1);
      }

  for(k=0;k<4;k++){
    do{
      ch_data=fgetc(ifp);
    }while(ch_data!='\n');
  }

  chp=(unsigned char*)image;
  for(i=0;i<H;i++){
    for(j=0;j<W;j++,chp++){
      fscanf(ifp,"%d",&int_data);
      *chp = (unsigned char) int_data;
    }
  }
  
  chp=(unsigned char*)image;
  ch_max = 0;
  for(k=0;k<H*W; k++,chp++){
    if(*chp > ch_max) ch_max= *chp;
  }
  
  fprintf(ofp,"P5\n");
  fprintf(ofp,"#FORMAT=PGM BINARY\n");
  fprintf(ofp,"%d %d\n",W,H);
  fprintf(ofp,"%d\n",ch_max);

  chp=(unsigned char*)image;
  for(k=0;k<H*W;k++,chp++){
    fprintf(ofp,"%c",*chp);
  }
  
  for(i=0;i<H;i++){
    for(j=0;j<W;j++){
      printf("%3d",image[i][j]);
    }
    printf("\n");
  }
  
  fclose(ofp);
  fclose(ifp);
  return(0);
}
  
  
