/* ptrsum.c : Calculate a sum of an array */

#include<stdio.h>

int arraysum2(int *ptr);

main(){
  int a[]={11,12,13,14,15,16,17,18,19,20,-1};
  int sum;
  sum=arraysum2(a);
  printf("合計 : %d\n",sum);
}

int arraysum2(int *ptr){
  int sum=0;
  for(;*ptr!=-1;ptr++){
    sum+=*ptr;
  }
  return(sum);
}
