/* dispstr2.c : display a string with a pointer */

#include<stdio.h>
void puts2(char *str);

main(){
  char *str="hello, world\n";
  puts2(str);
}

void puts2(char *str){
  while(*str!='\0'){ /* ここは while(*str) でも良い */
      putchar(*str++);
    }
}
