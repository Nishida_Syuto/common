#include<stdio.h>

main(){
  int array1[8]={10,11,12,13,14,15,16,17};
  int *p;
  int k;
  for(p=(int *)array1,k=0;k<8;k++,p++){
    *p+=100;
  }
  /* 配列array1への代入の確認 */
  for(k=0;k<8;k++){
    printf("array1[%d]=%d\n",k,array1[k]);
  }
  /* 配列array1への代入の確認(別方法) */
  for(p=(int *)array1,k=0;k<8;k++,p++){
    printf("array1[%d]=%d\n",k,*p);
  }
}
