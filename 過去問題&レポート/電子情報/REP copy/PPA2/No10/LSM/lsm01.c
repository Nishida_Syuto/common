#include<stdio.h>

#define N (10)
#define IMAX (3)
#define JMAX (4)

/*必要なプリプロセッサ実行例があれば追加*/

/*拡大係数行列を記憶する配列の宣言*/

double array[IMAX][JMAX];

/*sweepと同様に使用する関数定義を以下に書く*/

void PrintMat(void){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("%5.2f ",array[i][j]);
    }
    putchar('\n');
  }
  putchar('\n');
}

void SweepOut(void){
  int i,j,axis;
  double pivot,aik;
  for(axis=0;axis<IMAX;axis++){
    pivot=array[axis][axis];
    for(j=axis;j<JMAX;j++){
      array[axis][j]/=pivot;
    }
    for(i=0;i<IMAX;i++){
      if(i!=axis){
	aik=array[i][axis];
	for(j=0;j<JMAX;j++){
	  array[i][j]-=array[axis][j]*aik;
	}
      }
    }
  }
}

main(){ /*関数mainを構成するブロックの先頭*/
  double x[]={1.00,2.00,3.00,4.00,5.00,6.00,7.00,8.00,9.00,10.00};
  double y[]={592.23,578.91,562.91,521.31,485.38,416.22,363.66,278.65,200.62,114.31};
  double xxxx,xxx,xx,xi;
  double yxx,yx,yi;
  int i;
  double g;  

  /*総和の計算で使用する拡大係数行列arrayの要素の初期化*/
  
  
  array[0][0]=array[0][1]=0;
  array[0][2]=array[0][3]=0;
  array[1][2]=array[1][3]=array[2][3]=0;

  /*拡大係数行列arrayの計算*/

  for(i=0;i<N;i++){
    xi=x[i];yi=y[i];
    xx=xi*xi;xxx=xx*xi;xxxx=xxx*xi;yx=yi*xi;yxx=yx*xi;
    array[0][0]+=xxxx;array[0][1]+=xxx;
    array[0][2]+=xx;array[0][3]+=yxx;
    array[1][2]+=xi;array[1][3]+=yx;array[2][3]+=yi;
  }
  array[2][2]=N;
  array[1][0]=array[0][1];array[1][1]=array[0][2];
  array[2][0]=array[0][2];array[2][1]=array[1][2];

  /*拡大係数行列arrayで表される連立方程式を以下で解き、結果を表示しなさい*/

  //PrintMat();
  SweepOut();
  //PrintMat();

  g=-2*(array[0][3]);

  printf("a= %g ,b= %g ,c= %g ,g= %g \n",array[0][3],array[1][3],array[2][3],g);

}
