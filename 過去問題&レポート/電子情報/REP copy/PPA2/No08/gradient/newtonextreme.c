#include<stdio.h>
#include<math.h>

#define EPS (1.0E-12)

main(){
  double delx,dely;
  double x,y;
  double g,f,fx,fy,fxx,fyy,fxy,fyx,d;
  double error;
  int i;

  printf("Input x1 y1 = ");
  scanf("%lf %lf",&x,&y);
  for(i=1,error=2*EPS,delx=dely=0.0;
      error >=EPS; i++){ /*start of for block */
    x+=delx;y+=dely;
    /* No01 */ g=exp(-(x*x+y*y)/2);
    /* No02 */ f= -x*g;fx=(x*x-1)*g;
    /* No03 */fy=-y*f;
    /* No04 */error=fabs(fx)+fabs(fy);

    printf("i=%d,x=%g,y=%g,fx=%g,fy=%g,f=%g\n",i,x,y,fx,fy,f);

    /* For Next Step */
    /* No5-1 */ fxx=(x*x-3)*f;fyy=(y*y-1)*f;
    /* No5-2 */fxy=-y*fx;
    /* No6 */fyx=fxy;
    /* No7 */d=fxy*fyx-fxx*fyy;
    /* No8 */delx=(fx*fyy-fy*fxy)/d;dely=(fy*fxx-fx*fyx)/d;
  }/* end of for block */
  printf("Iteration=%d Error=%g:",i-1,error);
  /* Check the position (x,y) */
  if(d<0){
    printf("(%g,%g) is the extremum\n",x,y);
    return(0);
  }
  else{
    printf("(%g,%g) is NOT the extremum\n",x,y);
    return(-1);
  }
} /*end of main */
