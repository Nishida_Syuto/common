#include<stdio.h>
#include<string.h>
#include<math.h>

/* ファイル名 : mycalc0.c */
/* 使い方 : ./mycalc0 算術関数名 数値 */

main(int argc,char *argv[]){
  double x,y;
  char *fname;
  int n;

  /* 準備段階 */
  if(argc<3){
    return(-1); //arg2がない
  }
  if(argc>3){
    return(-2); //arg2以降に引数
  }
  n=sscanf(argv[2],"%lf",&x);
  if(n!=1){
    return(-5); //arg2の文字列の先頭が文字
  }



  /* 選択制御構造 */
  fname=argv[1];

  if(strcmp(fname,"exp")==0){ /* 関数==exp */
  /* 関数値の計算 */
  y= exp(x);
}

  else if(strcmp(fname,"sqrt")==0){ /* 関数==sqrt */
    /* 関数値の計算 */ 
    if(0<=x){
      y=sqrt(x);
    }else{
      return(-4); //関数の定義域外
    }
  }

  else if(strcmp(fname,"cos")==0){ /* 関数==cos */
    /* 関数値の計算 */
    y= cos(x);
  }

  else if(strcmp(fname,"acos")==0){ /* 関数==acos */
    if(-1<=x && x<=1){
      /* 関数値の計算 */
      y= acos(x);
    }else{
      return(-4); //関数の定義域外
    }
  }

  else if(strcmp(fname,"fabs")==0){ /* 関数==fabs */
    /* 関数値の計算 */
    y= fabs(x);
  } /* end of if */

  else{
    return(-3); //関数群のリスト外
  }

  /* 最終段階 */
  printf("%e\n",y);
  return(0); //関数の計算可能

}/* end of main */
