#include<stdio.h>
#include<string.h>
#include<math.h>

/* ファイル名 : mycalc0.c */
/* 使い方 : ./mycalc0 算術関数名 数値 */

main(int argc,char *argv[]){
  double x,y;
  char *fname;
  int n;

  /* 準備段階 */
  if(argc<3){
    return(-1);
  }
  n=sscanf(argv[2],"%lf",&x);
  if(n!=1){
    return(-2);
  }

  /* 選択制御構造 */
  fname=argv[1];
  if(strcmp(fname,"sqrt")==0){ /* 関数==sqrt */
    /* 関数値の計算 */
    y= sqrt(x) ;
  }
  else{
    return(-3);
  }

  /* 最終段階 */
  printf("%g\n",y);
  return(0);
}/* end of main */
