#include<stdio.h>
#include<string.h>
#include<math.h>

/* ファイル名 : mycalc0.c */
/* 使い方 : ./mycalc0 算術関数名 数値 */

main(int argc,char *argv[]){
  double x,y;
  char *fname;
  int n;

  /* 準備段階 */
  if(argc<3){
    return(-1);
  }
  n=sscanf(argv[2],"%lf",&x);
  if(n!=1){
    return(-2);
  }

  /* 選択制御構造 */
  fname=argv[1];
  if(strcmp(fname,"exp")==0){ /* 関数==exp */
  /* 関数値の計算 */
  y= exp(x);
}
  else if(strcmp(fname,"sqrt")==0){ /* 関数==sqrt */
    /* 関数値の計算 */
    y= sqrt(x) ;
  }
  else if(strcmp(fname,"cos")==0){ /* 関数==cos */
    /* 関数値の計算 */
    y= cos(x);
  }
  else if(strcmp(fname,"acos")==0){ /* 関数==acos */
    /* 関数値の計算 */
    y= acos(x);
  }
  else if(strcmp(fname,"fabs")==0){ /* 関数==fabs */
    /* 関数値の計算 */
    y= fabs(x);
  } /* end of if */
  else{

    return(-3);
  }

  /* 最終段階 */
  printf("%e\n",y);
  return(0);
}/* end of main */
