/*main1.c * examine main function */

#include<stdio.h>

int main(int argc,char *argv[]){
  int i;
  printf("コマンドライン引数は %d 個です.\n",argc);
  for(i=0;i<argc;i++){
    printf("第 %d 引数は [%s] です.\n",i,argv[i]);
  }
}
