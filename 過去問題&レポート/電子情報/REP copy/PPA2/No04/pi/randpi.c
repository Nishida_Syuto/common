#include<stdio.h>
#include<stdlib.h>
#include<time.h>

main(){

  int i,n,count=0;
  double z,x,y,rr;

  scanf("%d",&n);

  srand(time(NULL));

  for(i=0;i<n;i++){
    z=(double)rand()/(double)RAND_MAX;
    x=(z-0.5)*2;
    z=(double)rand()/(double)RAND_MAX;
    y=(z-0.5)*2;

    rr=x*x+y*y;
    
    if(rr<1){
      count++;
    }

  }

  printf("%f\n",4*(double)count/n);

  return(0);
}
