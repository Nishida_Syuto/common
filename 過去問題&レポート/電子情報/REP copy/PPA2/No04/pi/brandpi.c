#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "mymacro.h"

main(){

  int i,n,count=0;
  double x,y,rr;

  scanf("%d",&n);

  srand(time(NULL));

  for(i=0;i<n;i++){

    x=ARAND(1.0);

    y=ARAND(1.0);

    rr=x*x+y*y;
    
    if(rr<1){
      count++;
    }

  }

  printf("%f\n",4*(double)count/n);

  return(0);
}
