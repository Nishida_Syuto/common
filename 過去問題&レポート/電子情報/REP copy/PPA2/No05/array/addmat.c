/* addmat.c : calculate a sum of a pair of matrices */

#include<stdio.h>

void add(double c[2][2],double a[2][2],double b[2][2]);

main(){
  double a[2][2]={
    {4,3},
    {5,1}
  },
    b[2][2]={
      {1,0},
      {0,1}
    },
      c[2][2];

      add(c,a,b); /* C=A+B */

      printf("%f %f\n%f %f\n",c[0][0],c[0][1],c[1][0],c[1][1]);
 
     return(0);
}

void add(double c[2][2],double a[2][2],double b[2][2]){
  int i,j;
  for(i=0;i<2;i++){
    for(j=0;j<2;j++){
    c[i][j]=a[i][j]+b[i][j];
    }
  }
}
