/* mat33addeval.c : calculate a sum of a pair of matrices */

#include<stdio.h>

void mat33add(double c[3][3],double a[3][3],double b[3][3]);

main(){
  double a[3][3]={
    {4,3,2},
    {5,4,3},
    {6,5,4}
  },
    b[3][3]={
      {1,2,3},
      {2,3,4},
      {3,4,5}
    },
      c[3][3];

      mat33add(c,a,b); /* C=A+B */

      printf("%f %f %f\n%f %f %f\n%f %f %f\n",c[0][0],c[0][1],c[0][2],c[1][0],c[1][1],c[1][2],c[2][0],c[2][1],c[2][2]);
 
     return(0);
}
