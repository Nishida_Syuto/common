/* arraysumD.c : calculate a sum of an array */

#include<stdio.h>

double arraysum(double a[],int n){
  double sum=0;
  int i;
  for(i=0;i<n;i++){
    sum+=a[i];
  }
  return(sum);
}
