/* arraysum.c : calculate a sum of an array */

#include<stdio.h>

int arraysum(int array[],int n);

main(){
  int a[]={11,12,13,14,15,16,17,18,19,20};
  int n=10,sum;
  sum=arraysum(a,n);
  printf("合計:%d\n",sum);

  return(0);
}

int arraysum(int a[],int n){
  int sum=0,i;
  for(i=0;i<n;i++){
    sum+=a[i];
  }
  return(sum);
}
