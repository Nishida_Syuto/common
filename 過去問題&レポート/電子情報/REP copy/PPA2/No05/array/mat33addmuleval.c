#include<stdio.h>

void mat33add(double c[3][3],double a[3][3],double b[3][3]);
void mat33mul(double f[3][3],double d[3][3],double e[3][3]);

main(){
  double a[3][3]={
    {4,3,2},
    {5,4,3},
    {6,5,4}
  },
    b[3][3]={
      {1,2,3},
      {2,3,4},
      {3,4,5}
    },
      c[3][3]={
	{-4,3,2},
	{5,-4,3},
	{6,5,-4}
      },
	d[3][3]={
	  {1,2,-3},
	  {2,-3,4},
	  {-3,4,5}
	},
	  e[3][3],f[3][3],g[3][3];

	  mat33add(e,a,b);
	  mat33add(f,c,d);
	  mat33mul(g,e,f);

 printf("%f %f %f\n%f %f %f\n%f %f %f\n",g[0][0],g[0][1],g[0][2],g[1][0],g[1][1],g[1][2],g[2][0],g[2][1],g[2][2]);
 
     return(0);
}
