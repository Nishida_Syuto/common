void mat33mul(double c[3][3],double a[3][3],double b[3][3]){
  int i,j,k;
  double sum;
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      for(k=0,sum=0;k<3;k++){
	  sum+=a[i][k]*b[k][j];
	}
	c[i][j]=sum;
    }
  }
}
