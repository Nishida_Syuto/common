/* sweep2.c : Sweepout by Gauss-Jordan method */
#include<stdio.h>

#define IMAX (4)
#define JMAX (5)

double array[IMAX][JMAX];
double arrayA[IMAX][JMAX],arrayB[IMAX],arrayS[IMAX],arrayC[IMAX];

void PrintMat(void){
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      printf("%5.2f ",array[i][j]);
    }
    putchar('\n');
  }
  putchar('\n');
}

void SweepOut(void){
  int i,j,axis;
  double pivot,aik;
  for(axis=0;axis<IMAX;axis++){
    pivot=array[axis][axis];
    for(j=axis;j<JMAX;j++){
      array[axis][j]/=pivot;
    }
    for(i=0;i<IMAX;i++){
      if(i!=axis){
	aik=array[i][axis];
	for(j=0;j<JMAX;j++){
	  array[i][j]-=array[axis][j]*aik;
	}
      }
    }
  }
}

main(){
  
  int i,j;
  for(i=0;i<IMAX;i++){
    for(j=0;j<JMAX;j++){
      scanf("%lf",&array[i][j]);
    }
  }

  for(i=0;i<IMAX;i++){
    for(j=0;j<IMAX;j++){ // 注:JMAXをIMAXに変更
      arrayA[i][j]=array[i][j];
    }
    arrayB[i]=array[i][j]; // 注:この時点でj=IMAX
  }

  PrintMat();
  SweepOut();

  // 注:arrayの第一軸をIMAXに固定
  for(i=0;i<IMAX;i++){arrayS[i]=array[i][IMAX];}

  {double sum;
    for(i=0;i<IMAX;i++){
      for(j=0,sum=0;j<IMAX;j++){ // 注:JMAXをIMAXに変更
	sum+=arrayA[i][j]*arrayS[j];
      }
      arrayC[i]=sum-arrayB[i];
    }
  }

  PrintMat();

  for(i=0;i<IMAX;i++){
    printf("%g",arrayC[i]);
    putchar('\n');
  }
}
