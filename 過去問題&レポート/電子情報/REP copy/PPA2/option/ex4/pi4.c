// pi4.c : M_PI/4 by expansion

#include<math.h>

double pi4(int n){
  int i;
  double ans=1.0,term=1.0;
  for(i=1;i<n;i++){
    term*=(pow(-1,i))/(2*i+1);
    ans+=term;
  }
  return(ans);
}
