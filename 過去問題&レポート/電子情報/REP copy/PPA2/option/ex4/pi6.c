#include<math.h>

double pi6(int n){

  int i,a;
  double ans=0.2,term=1.0;
  for(i=1;i<n;i++){
    a=2*i-1;
    term*=(pow(-1,i-1))/(a*(pow(a,4)+4));
    ans+=term;
  }
  return(ans);
}
