#include<stdio.h>
#include<math.h>

#define EPS (1.0e-10)
#define ITMAX (1000)

int main(int argc,char *argv[]){
  int i,k,itmax;
  double xi,xinext,fx,a,eps,x0;

  sscanf(argv[argc-1],"%lf",&a);
  x0=a;
  eps=EPS;
  itmax=ITMAX;
  k=1;
  while(k<argc-1){ /* オプションがついていた時 */
    if(strcmp(argv[k],"-E")==0){ /* 収束判定基準値 */
      k++;
      sscanf(argv[k],"%lf",&eps);
    }
    else if(strcmp(argv[k],"-I")==0){ /* 初期値 */
      k++;
      sscanf(argv[k],"%lf",&x0);
    }
    else if(strcmp(argv[k],"-L")==0){ /* 反復回数の上限値 */
      k++;
      sscanf(argv[k],"%d",&itmax);
    }
    else{return(255);
    }
    k++;
  } /* End of while ブロック */

 printf("収束判定基準値=%e 初期値=%e 反復回数の上限値=%d\n",eps,x0,itmax);

 /* 以下で方程式を解く */
 i=0;xi=x0;
 do{
   xinext=(xi+a/xi)/2;
   fx=exp(-xinext)-a;
   i++;xi=xinext;

   if(i+1>itmax){
     printf("i=%d xinext=%e fx=%e a=%f \n",i,xinext,fx,a);
     return(1);
   }
 }
 while(fx<=-eps || fx>=eps);

  printf("i=%d xinext=%e fx=%e a=%f \n",i,xinext,fx,a);
 
 return(0);
}
