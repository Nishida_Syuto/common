// exp4.c : exp by Maclaurin expansion

double exp4(double x,int n){
  int i;
  double ans=1.0,term=x;
  for(i=1;i<=n;i++){
    term*=x/i;
    ans+=term;
  }
  return(ans);
}
