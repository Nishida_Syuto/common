/* sin3.c :sine by Maclaurin expansion */
double sin3(double x){ /* sin3()の計算 */
  int i,n=5;
  double ans=x,term=x,mx2=-x*x;
  for(i=1;i<=n;i++){
    term*=mx2/(2*i*(2*i+1));
    ans+=term;
  }
  return(ans);
}
