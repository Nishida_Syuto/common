#include<stdio.h>
double power1(double x,int n);
double power2(double x,int n);


int main(){
  int n;
  double x,p,q;

  printf("x,nを入力:");
  scanf("%lf %d",&x,&n);

  p=power1(x,n);
  q=power2(x,n);

  printf("再帰なし: %lf\n",p);
  printf("再帰あり: %lf\n",q);

  return(0);
}
