/* fact1.c :caiculate factorials */
#include<stdio.h>
double fact1(int n);
double fact2(int n);

main(){
  int a;
  printf("a = ");
  scanf("%d",&a);
  printf("%d! = %f (def.1)\n",a,fact1(a));
  printf("%d! = %f (def.2)\n",a,fact2(a));
  return(0);
}

double fact1(int n){ /* 定義1による階乗の関数定義 */
  int i;
  double ans=1.0;
  for(i=1;i<=n;i++){
    ans*=i;
  }
  return(ans);
}

double fact2(int n){ /* 定義2による階乗の関数定義 */
  if(n==0){
    return(1);
  }else{
    return(n*fact2(n-1));
  }
}
