double power2(double x,int n){ /* 定義2によるべき乗の関数定義 */
  if(n==0){
    return(1);
  }
  if(n>0){
    return(x*power2(x,(n-1)));
  }
}
