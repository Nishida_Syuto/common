#include<stdio.h>
#include<math.h>

double sin3(double x);

int main(){

  int i;
  double w,x,y,z;

  printf("x=?       sin3(x)    sin(x)     |sin3(x)-sin(x)|\n");

  for(i=0;i<=16;i++){

    w=i*(M_PI/8);
    x=sin3(w);
    y=sin(w);
    z=((x-y)>0)?(x-y):(y-x);

    printf("%f  %-+4.6f  %-+4.6f  %f\n",w,x,y,z);
 
  }

  return(0);
}
