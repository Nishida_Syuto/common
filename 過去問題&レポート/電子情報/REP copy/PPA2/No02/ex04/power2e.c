double power2e(double x,int n){
  if(n==0){
    return(1);
  }
  if(n<0){
    int m;
    m=-n;
    return(1/(x*power2e(x,m-1)));
  }
}
