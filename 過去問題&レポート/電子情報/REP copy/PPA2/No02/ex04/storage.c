/*storage.c :compare auto/static class */
#include<stdio.h>
void sub(void);

int extern_var=0;

main(){
  int i;
  for(i=1;i<=10;i++){ /*　sub()を10回呼ぶ　*/
    sub();
  }
  return(0);
}

void sub(void){
  auto int auto_var=0; /*　auto変数　*/
  static int static_var=0; /*　static変数　*/
  extern int extern_var; /*　extern変数　*/
  auto_var++;
  static_var++;
  extern_var++;
  printf("auto = %d, static = %d, extern = %d\n",
auto_var,static_var,extern_var);
}
