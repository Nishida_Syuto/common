#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>

#define H (24)
#define W (32)

int main(int argc,char *argv[]){
   FILE *ifp;
   unsigned char image[H][W],*chp,ch_data, *image_data;
   int k,int_data,i,j,width,height,ch_max;
   char cbuf[256];

   ifp=fopen(argv[1],"r");
   if(ifp==NULL){
     fprintf(stderr,"fopen error %s\n",argv[1]);
     exit(-1);
   }
   
   fscanf(ifp,"%s",cbuf);
   if(strcmp(cbuf,"P5")!=0){
     fprintf(stderr,"magic number != P5\n");
     exit(255);
   }
   
   for(k=0;k<2;k++){
     do{
       ch_data=fgetc(ifp);
     }
     while(ch_data!='\n');
   }

   fscanf(ifp,"%d %d",&width,&height);
   if(width<0||width>USHRT_MAX)exit(254);
   if(height<0||height>USHRT_MAX)exit(254);

   fscanf(ifp,"%d", &ch_max);
   if(ch_max<0||ch_max>255)exit(253);

   do{
     ch_data=fgetc(ifp);
   }while(ch_data!='\n');
   
   image_data=(unsigned char *)malloc(sizeof(unsigned char)*width*height);
   if(image_data==NULL){
     fprintf(stderr,"malloc error\n");
     exit(252);
   }
   
   chp=image_data;
   for(i=0;i<height;i++){
     for(j=0;j<width;j++,chp++){
       fscanf(ifp,"%c",chp);
     }
   }

   chp=image_data;
   for(i=0;i<height;i++){
     for(j=0;j<width;j++,chp++){
       printf("%3d",*chp);
     }
     printf("\n");
   }
   fclose(ifp);
   return(0);
}
