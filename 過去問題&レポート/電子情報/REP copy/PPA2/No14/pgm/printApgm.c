#include<stdio.h>
#include<stdlib.h>

#define H (24)
#define W (32)

 int main(int argc,char *argv){
  FILE *ifp;
  char infile[]="imageA32.pgm";
  unsigned char image[H][W];
  unsigned char *chp,ch_data;
  int k,int_data,i,j;
  ifp=fopen(infile,"r");
  if(ifp==NULL){
    fprintf(stderr,"fopen error %s\n",infile);
    exit(-1);
  }
  for(k=0;k<4;k++){
    do{
      ch_data=fgetc(ifp);
    }
    while(ch_data!='\n');
  }
  chp=(unsigned char *)image;
  for(i=0;i<H;i++){
    for(j=0;j<W;j++,chp++){
    fscanf(ifp,"%d",&int_data);
    *chp=(unsigned char)int_data;
  }
}

for(i=0;i<H;i++){
  for(j=0;j<W;j++){
    printf("%3d",image[i][j]);
  }
  printf("\n");
 }
fclose(ifp);
return(0);
}
