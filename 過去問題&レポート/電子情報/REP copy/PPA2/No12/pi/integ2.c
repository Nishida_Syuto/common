#include<stdio.h>
#include<math.h>

double func(double x);
double integral2(double (*func)(),double a,double b,int n);

main(){
  double a,b;
  int n;
  printf("積分区間[a,b]と区間数n a,b,n:");
  scanf("%lf,%lf,%d",&a,&b,&n);
  printf("2次のシンプソンの公式 pi2=%20.17f\n",integral2(func,a,b,n));
  printf("pi2-pi=%20.17f\n",integral2(func,a,b,n)-M_PI);
}

double func(double x){
  return(4.0/(1.0+x*x));
}

double integral2(double (*func)(),double a,double b,int n){
  double h=(b-a)/n,suma=0.0,sumb=0.0,x,answer;
  int i;
  for(i=1,x=a+h;i<n;i=i+2,x+=2*h){
    suma+=func(x);
  }
  for(i=2,x=a+2*h;i<n-1;i=i+2,x+=2*h){
    sumb+=func(x);
  }
  answer=h/3*(func(a)+func(b)+4*suma+2*sumb);
  return(answer);
}
