#include<stdio.h>
#include<math.h>

double func(double x);
double integral1(double (*func)(),double a,double b,int n);

main(){
  double a,b;
  int n;
  printf("積分区間[a,b]と区間数n a,b,n:");
  scanf("%lf,%lf,%d",&a,&b,&n);
  printf("台形公式 pi1=%20.17f\n",integral1(func,a,b,n));
  printf("pi1-pi=%20.17f\n",integral1(func,a,b,n)-M_PI);
}

double func(double x){
  return(4.0/(1.0+x*x));
}

double integral1(double (*func)(),double a,double b,int n){
  double h=(b-a)/n,sum=0.0,x,answer;
  int i;
  for(i=1,x=a+h;i<n;i++,x+=h){
    sum+=func(x);
  }
  answer=h*(((func(a)+func(b))/2)+sum);
  return(answer);
}
