/* funcptr1.c : examine function pointer definition */

#include<stdio.h>
#include<math.h>

#define PI (3.14159265)

main(){
  double (*func)();
  double radian=PI/4;
  func=sin;
  printf("sin(%f)=%f\n",radian,sin(radian));
  printf("func(%f)=%f\n",radian,func(radian));
}
