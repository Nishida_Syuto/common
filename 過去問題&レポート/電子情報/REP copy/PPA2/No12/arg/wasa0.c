/* wasa0.c : a sample for a call by address */

#include<stdio.h>

void wasa(int a,int b,int *wa,int *sa);

void wasa0(int x,int y,int *add,int *sub);

main(){
  int a,b,wa,sa;
  printf("Input a pair of numbers a,b :");
  scanf("%d,%d",&a,&b);
  printf("Before wasa : &a=%p &b=%p &wa=%p &sa=%p\n",&a,&b,&wa,&sa);
  wasa(a,b,&wa,&sa);
  printf("AFTER wasa : a+b=%d a-b=%d\n",wa,sa);

  printf("Before wasa0 : &a=%p &b=%p &wa=%p &sa=%p\n",&a,&b,&wa,&sa);
  wasa0(a,b,&wa,&sa);
  printf("AFTER wasa0 : a+b=%d a-b=%d\n",wa,sa);

}

void wasa(int a,int b,int *wa,int *sa){

  printf("In wasa : &a=%p &b=%p wa=%p sa=%p\n",&a,&b,wa,sa);

  printf("In wasa : a=%d b=%d\n",a,b);

  *wa=a+b;
  *sa=a-b;
}

void wasa0(int x,int y,int *add,int *sub){

  printf("In wasa0 : &add=%p &sub=%p\n",&add,&sub);
  printf("In wasa0 : &x=%p &y=%p add=%p sub=%p\n",&x,&y,add,sub);
  printf("In wasa0 : x=%d y=%d\n",x,y);

  *add=x+y;
  *sub=x-y;
}
