#include<stdio.h>
#include<math.h>

#define N (256)

int statis(double *array,int n,double *ave,double *sigma);

main(){
  int i,k;
  double a[N];
  double ave,sigma;

  for(i=0,k=1;i<N;i=k++){
    a[i]=k;
  }
  
  statis(a,N,&ave,&sigma);

  printf("arv=%f sigma=%f\n",ave,sigma);

}

  int statis(double *array,int n,double *ave,double *sigma){
    
    double m,s;int i;
    int sum=0;
    
    for(i=0;i<n;i++){
      m+=array[i];
    }
    *ave=m/n;

    for(i=0;i<n;i++){
      s+=pow(array[i]-*ave,2);
    }
    s/=n-1;
    *sigma=sqrt(s);
  }

