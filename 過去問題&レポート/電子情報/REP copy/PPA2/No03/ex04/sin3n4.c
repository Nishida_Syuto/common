#include<stdio.h>
#include<math.h>
double sin3n(double x,int n);

int main(){

  double x,pi,i;

  pi=M_PI;

  for(i=-pi;i<=pi;i=i+2*pi/360){
    
    x=sin3n(i,4);

    printf("%-+lf     %-+lf\n",i,x);

  }

  return(0);
}
