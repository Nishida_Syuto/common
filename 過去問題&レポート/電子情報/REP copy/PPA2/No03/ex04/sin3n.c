/* sin3.c :sine by Maclaurin expansion */
double sin3n(double x,int n){ /* sin3() */
  int i;
  double ans=x,term=x,mx2=-x*x;
  for(i=1;i<=n;i++){
    term*=(mx2)/(2*i*(2*i+1));
    ans+=term;
  }
  return(ans);
}
