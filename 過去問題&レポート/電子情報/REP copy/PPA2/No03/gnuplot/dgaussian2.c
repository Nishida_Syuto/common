#include<stdio.h>
#include<math.h>

int main(){

  int i;
  double x,y;

  for(i=-35;i<=35;i++){
    x=0.1*i;
    y=-x*120*exp((-x*x)/2);
    printf("%-+4.1f   %e\n",x,y);
  }

  return(0);
}
