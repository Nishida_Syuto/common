#include<stdio.h>
#include<math.h>

int main(){

  int i,n,j,k;
  double x,gx;
  

  printf("y=   0");

  for(k=1;k<=12;k++){
    printf("        %d",10*k);
  }
  printf("\n");

  for(i=-35;i<=35;i++){
    x=0.1*i;
    gx=120*exp((-x*x)/2);
    n=(int)(gx+0.5);

    printf("x=%-+4.1f",x);
    for(j=0;j<n;j++){
      printf(" ");
    }
    printf("*\n");
  }

  printf("y=   0\n");

  for(k=1;k<=12;k++){
    printf("        %d",10*k);
  }
  printf("\n");


  return(0);
}
