#include<stdio.h>

#define EPS (1.0e-10)

int main(){ /* main ブロック */
  int i;
  double xi,xinext,a,dxi;

  /* 初期化 */ a=1,i=0,xi=5;
  do{ /* do ブロック */
     xinext=(xi+a/xi)/2;
     dxi=xinext-xi;
     printf("i=%d xi=%e xinext=%e dxi=%e\n",i,xi,xinext,dxi);
      /* 更新式 */i++;xi=xinext;
  } /*End of do ブロック */
  while( /* 論理式 */ dxi<=-EPS || dxi>=EPS);


  return(0);

} /* End of main ブロック */
