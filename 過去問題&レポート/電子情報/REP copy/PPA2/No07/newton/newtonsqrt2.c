#include<stdio.h>

#define EPS (1.0e-10)

int main(int argc,char *argv[]){ /* main ブロック */
  int i;
  double xi,xinext,fx,a;

  sscanf(argv[1],"%lf",&a);


  /* 初期化 */ i=0,xi=a;
  do{ /* do ブロック */
     xinext=(xi+a/xi)/2;
     fx=xinext*xinext-a;
     printf("i+1=%d xinext=%e fx=%e\n",i+1,xinext,fx);
      /* 更新式 */i++;xi=xinext;
  } /*End of do ブロック */
  while( /* 論理式 */ fx<=-EPS || fx>=EPS);


  return(0);

} /* End of main ブロック */
