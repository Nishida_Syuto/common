import java.awt.*;
import javax.swing.*;
import java.nio.charset.Charset;

class ex17 {

  static class Ex17 extends JPanel {
    Font[] fonts;
    int ord = 596;  // URW Chancery L Medium Italic

    Ex17 (int ord) {
      fonts = GraphicsEnvironment.getLocalGraphicsEnvironment ().getAllFonts ();
      if ((0 <= ord) && (ord < fonts.length)) this.ord = ord;
      setPreferredSize (APlot.A4);
    }

    public void paintComponent (Graphics g) {
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      String fontname = fonts[ord].getFontName (),
             fontNo = String.format ("%d", ord);
      Charset isoLatin1 = Charset.forName ("ISO-8859-1");

      a.setCoordinate (30, 30, 160, 230, 0, 8, 0, 32);
      a.setFont ("Serif", 12); a.drawString (fontNo, 0, 32, 18, 24, -1);
      a.setFont ("Serif", 18); a.drawString (fontname, 0, 32, 22, 24);
      for (int i = 0; i < 256; i++) {
        double x = i/32, y = 31-(i%32);
        if (y == 31) {
          a.setFont ("Serif", 12); a.drawString ("Hex Char", x, 32);
        }
        String hex = String.format ("%02x",i),
               c = new String (new byte[] {(byte) i}, isoLatin1);
        a.setFont ("Serif", 12); a.drawString (hex, x, y, 5);
        a.setFont (fontname, 15); a.drawString (c, x, y, 30);
      }
    }
  }

  public static void main (String args[]) {
    int ord = -1;
    if (args.length > 0) {
      try {
        ord = Integer.parseInt (args[0]);
      } catch (NumberFormatException e) {
        System.err.println ("\"" + args[0] + "\" is not Integer.");
        System.exit(1);
      }
    }
    APage p = new APage (true, new Ex17 (ord), "ex17.ps");
    //System.exit (0);
  }
}
