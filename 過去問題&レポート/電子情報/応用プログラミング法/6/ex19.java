import java.awt.*;
import javax.swing.*;

class ex19 {

  static class Ex19 extends JPanel {
    Font[] fonts;
    char ch = '\u03B1'; // Unicode  (greek small alpha)

    Ex19 () {
      fonts = GraphicsEnvironment.getLocalGraphicsEnvironment ().getAllFonts ();
      setPreferredSize (APlot.A4);
    }

    public void paintComponent (Graphics g) {
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      double xleft = 30, ytop = 255;
      a.setFont ("SansSerif", 14);
      a.drawString (String.format ("Unicode:%04x", (int) ch),
        xleft, ytop, 0, 16);
      for (int i = 0; i < fonts.length; i++) {
        double x = xleft+12*(i/50), y = ytop-5*(i%50);
        String fontname = fonts[i].getFontName (),
               fontNo = String.format ("%d", i);
        a.setFont ("Serif", 8); a.drawString (fontNo, x, y, 0, 0, -1);
        a.setFont (fontname, 14);
        a.drawString (Character.toString (ch), x, y, 3);
      }
    }
  }

  public static void main (String args[]) {
    APage p = new APage (true, new Ex19 (), "ex19.ps");
    //System.exit (0);
  }
}
