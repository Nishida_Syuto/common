import java.awt.*;
import javax.swing.*;

class ex15{
    static class Ex15 extends JPanel{
	Font [] fonts;
	int[] ord = {
	    498,
	    490,
	    93,
	    97,
	    412,
	    57,
	    54,
	    56,
	    55,
	    596,
	    327};

	Ex15(){
	    fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
	    for(int i=0; i<fonts.length;i++)
		System.out.format("%4d %s\n",i,fonts[i].getFontName() );
	    setPreferredSize(APlot.A4);
	}

	public void paintComponent(Graphics g){
	    Graphics2D g2 =(Graphics2D)g;
	    APlot a = new APlot (this,g2);

	    String text = "The five boxing wizards jump quickly. 0123456789";
	    double xtop = 65, ytop=265,ystep=5.5;
	    for(int i=0;i<ord.length;i++){
		double y=ytop-ystep*i;
		String fontname = fonts[ord[i]].getFontName();
		a.setFont("Serif",10.5);
		a.drawString(fontname,xtop,y,0,0,-1);
		a.setFont("Serif",9);
		a.drawString(String.format("%d",ord[i]),xtop+7,y,0,0,-1);
		a.setFont(fontname,11.5);
		a.drawString(text,xtop+9,y);
	    }
	}
    }

    public static void main (String arg[]){
	APage p= new APage (true,new Ex15(),"ex15.ps");
	//System.exit(0);
    }
}