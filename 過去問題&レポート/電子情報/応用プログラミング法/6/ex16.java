import java.awt.*;
import javax.swing.*;

class ex16{

 static class Ex16 extends JPanel{
  Font[] fonts;
     int[] ord = {327,317};

   Ex16(){
	fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
 	setPreferredSize(APlot.A4);
}

public void paintComponent(Graphics g) {

	Graphics2D g2 = (Graphics2D) g;
	APlot a = new APlot (this, g2);

	String text = "nihongo 日本語　にほんご　ニホンゴ　祇　箸";

	double xtop = 70,ytop = 265,ystep = 7;
	for(int i=0;i<ord.length ;i++){
	  double y = ytop-ystep*i;
	  String fontname = fonts[ord[i]].getFontName();

	  a.setFont("serif",10.5);
	  a.drawString(fontname,xtop,y,0,0,-1);
	  a.setFont("serif",9);
	  a.drawString(String.format("%d",ord[i]),xtop+7,y,0,0,-1);
	  a.setFont("serif",13);
	  a.drawString(text,xtop+9,y);
} 
}
}




public static void main (String args[]){
  APage p = new APage (true,new Ex16(),"ex16.ps");
}
}
