import java.awt.*;
import javax.swing.*;

class ex18 {

  static class Ex18 extends JPanel {
    Font[] fonts;
    int[] ord = {443, 77, 98};
    char[][] chars = {                                           // Unicode
     {'\u03B1', '\u03B2', '\u03B3', '\u03B4', '\u03B5', '\u03B6',
      '\u03B7', '\u03B8', '\u03B9', '\u03BA', '\u03BB', '\u03BC',
      '\u03BD', '\u03BE', '\u03BF', '\u03C0', '\u03C1', '\u03C2',
      '\u03C3', '\u03C4', '\u03C5', '\u03C6', '\u03C7', '\u03C8', '\u03C9',
      '\u0394', '\u0398', '\u039B', '\u03A0', '\u03A3', '\u03A9'},
     {'\u03B1', '\u03B2', '\u03B3', '\u03B4', '\u03B5', '\u03B6',
      '\u03B7', '\u03B8', '\u03B9', '\u03BA', '\u03BB', '\u03BC',
      '\u03BD', '\u03BE', '\u03BF', '\u03C0', '\u03C1', '\u03C2',
      '\u03C3', '\u03C4', '\u03C5', '\u03C6', '\u03C7', '\u03C8', '\u03C9',
      '\u0394', '\u0398', '\u039B', '\u03A0', '\u03A3', '\u03A9',
      '\u2620', '\u2622', '\u2623', '\u2624', '\u262A', '\u2615',
      '\u2696', '\u269C', '\u267F', '\u2614', '\u266B', '\u2668',
      '\u2603'},
     {'\u2734', '\u2723', '\u2748', '\u2740', '\u2764', '\u2766',
      '\u27A4', '\u270C', '\u261E', '\u2702', '\u270D', '\u270F',
      '\u2714', '\u266B', '\u2668', '\u2603'}};

    Ex18 () {
      fonts = GraphicsEnvironment.getLocalGraphicsEnvironment ().getAllFonts ();
      setPreferredSize (APlot.A4);
    }

    public void paintComponent (Graphics g) {
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      double xleft = 30, ytop = 265, y = ytop;
      for (int i = 0; i < ord.length; i++) {
        String fontname = fonts[ord[i]].getFontName (),
               fontNo = String.format ("%d", ord[i]);
        a.setFont ("Serif", 12); a.drawString (fontNo, xleft, y, 18, 0, -1);
        a.setFont ("Serif", 14); a.drawString (fontname, xleft, y, 22);
        y -= 7;
        for (int j = 0; j < chars[i].length; j++) {
          a.setFont ("Serif", 7.5); 
          a.drawString (String.format("%04x", (int) chars[i][j]), xleft, y,
            39+35*(j%12), 0, -1);
          a.setFont (fontname, 17);
          a.drawString (Character.toString (chars[i][j]), xleft, y,
            40+35*(j%12));
          if ((j%12) == 11) y-= 9;
        }
        if ((chars[i].length%12) != 0) y-= 9;
      }
    }
  }

  public static void main (String args[]) {
    APage p = new APage (true, new Ex18 (), "ex18.ps");
    //System.exit (0);
  }
}
