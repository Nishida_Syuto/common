import javax.swing.JComponent;
import javax.print.attribute.standard.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.text.AttributedCharacterIterator;

public class APlot {
  private Graphics2D g2;
  private JComponent c;
  private double width, height, xs, ys, xlen, ylen, x0, x1, y0, y1, rx, ry;
  private AffineTransform at;
  private static double pt = 72/25.4;  // 1 inch = 25.4 mm = 72 point
  public static float A0x = MediaSize.ISO.A0.getX (MediaSize.MM),
                      A0y = MediaSize.ISO.A0.getY (MediaSize.MM),
                      A1x = MediaSize.ISO.A1.getX (MediaSize.MM),
                      A1y = MediaSize.ISO.A1.getY (MediaSize.MM),
                      A2x = MediaSize.ISO.A2.getX (MediaSize.MM),
                      A2y = MediaSize.ISO.A2.getY (MediaSize.MM),
                      A3x = MediaSize.ISO.A3.getX (MediaSize.MM),
                      A3y = MediaSize.ISO.A3.getY (MediaSize.MM),
                      A4x = MediaSize.ISO.A4.getX (MediaSize.MM),
                      A4y = MediaSize.ISO.A4.getY (MediaSize.MM);
  public static Dimension 
    A0 = toPt (A0x, A0y),  A0landscape = toPt (A0y, A0x),  // pt
    A1 = toPt (A1x, A1y),  A1landscape = toPt (A1y, A1x),  // pt
    A2 = toPt (A2x, A2y),  A2landscape = toPt (A2y, A2x),  // pt
    A3 = toPt (A3x, A3y),  A3landscape = toPt (A3y, A3x),  // pt
    A4 = toPt (A4x, A4y),  A4landscape = toPt (A4y, A4x);  // pt

  public static Dimension toPt (double width, double height) {
    return new Dimension
      ((int) Math.ceil(width*pt), (int) Math.ceil (height*pt));  // mm to pt
  }


  APlot (JComponent c, Graphics2D g2) {
    this.g2 = g2; this.c = c;
    width = c.getWidth (); height = c.getHeight ();
    double w = width/pt, h = height/pt;
    setCoordinate (0, 0, w, h, 0, w, 0, h);
  }

  public void setCoordinate (double xs, double ys, double xlen, double ylen,
    double x0, double x1, double y0, double y1) {
// xs, ys, xlen, ylen : mm
// x0, x1, y0, y1     : user coordinate
    this.xs = xs*pt; this.ys = height-ys*pt;
    this.xlen = xlen*pt; this.ylen = ylen*pt;
    this.x0 = x0; this.x1 = x1; rx = this.xlen/(x1-x0);
    this.y0 = y0; this.y1 = y1; ry = this.ylen/(y1-y0);
    at = new AffineTransform (); at.translate (this.xs, this.ys);
    at.scale (rx, -ry); at.translate (-x0, -y0); 
  }

  private double px (double x) {return xs + rx*(x-x0);}
  private double py (double y) {return ys - ry*(y-y0);}


  public void clipOn (double x0, double x1, double y0, double y1) {
    double px0 = px (x0), py1 = py (y1);
    g2.setClip (c.getVisibleRect());
    g2.clip (new Rectangle2D.Double (px0, py1, px(x1)-px0, py(y0)-py1));
  }

  public void clipOn (double[] x, double[] y) {
    int start = 0, end = x.length;
    Path2D.Double path = new Path2D.Double ();
    path.moveTo (px(x[start]), py(y[start]));
    for (int i = start+1; i < end; i++) path.lineTo (px(x[i]), py(y[i]));
    path.closePath();
    g2.clip (path);
  }

  public void clipOn (Shape s) {
    g2.clip (at.createTransformedShape (s));
  }

  public void clipOn (Shape s, double x, double y) {
    clipOn (0, s, x, y, 0, 0, 0, 0);
  }

  public void clipOn (Shape s, double x, double y, double dx) {
    clipOn (0, s, x, y, dx, 0, 0, 0);
  }

  public void clipOn (Shape s, double x, double y, double dx, double dy) {
    clipOn (0, s, x, y, dx, dy, 0, 0);
  }

  public void clipOn (Shape s, double x, double y, double dx, double dy,
    double offsetx) {
    clipOn (0, s, x, y, dx, dy, offsetx, 0);
  }

  public void clipOn (Shape s, double x, double y, double dx, double dy,
    double offsetx, double offsety) {
    clipOn (0, s, x, y, dx, dy, offsetx, offsety);
  }

  public void clipOn (double angle, Shape s, double x, double y) {
    clipOn (angle, s, x, y, 0, 0, 0, 0);
  }

  public void clipOn (double angle, Shape s, double x, double y, double dx) {
    clipOn (angle, s, x, y, dx, 0, 0, 0);
  }

  public void clipOn (double angle, Shape s, double x, double y,
    double dx, double dy) {
    clipOn (angle, s, x, y, dx, dy, 0, 0);
  }

  public void clipOn (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx) {
    clipOn (angle, s, x, y, dx, dy, offsetx, 0);
  }

  public void clipOn (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx, double offsety) {
    double rr = Math.toRadians (angle), xx = px(x) + dx, yy = py(y) - dy;
    if ((offsetx != 0) || (offsety != 0)) {
      Rectangle2D bounds = s.getBounds2D ();
      double ww = offsetx*bounds.getWidth (), hh = offsety*bounds.getHeight (),
             cc = Math.cos (rr), ss = Math.sin (rr);
      xx += cc*ww-ss*hh; yy -= ss*ww+cc*hh;
    }
    AffineTransform at = new AffineTransform ();
    at.translate (xx, yy); if (rr != 0) at.rotate (-rr);
    g2.clip (at.createTransformedShape (s));
  }


  public void clipOff () {g2.setClip (c.getVisibleRect());}

  public void setFont (String name, double size) {
    g2.setFont (new Font (name, Font.PLAIN, 1).deriveFont ((float) size));
  }

  public void setFont (String name, int style, int size) {
    Font font = new Font (name, style, size);
    Hashtable <TextAttribute, Object> map
      = new Hashtable <TextAttribute, Object> ();
    map.put (TextAttribute.WIDTH, TextAttribute.WIDTH_REGULAR);
    font = font.deriveFont (map);
    g2.setFont (font);
  }


  public void drawStringC (String str, double x, double y) {
    drawStringC (0, str, x, y, 0, 0);
  }

  public void drawStringC (String str, double x, double y, double dx) {
    drawStringC (0, str, x, y, dx, 0);
  }

  public void drawStringC (double angle, String str, double x, double y) {
    drawStringC (angle, str, x, y, 0, 0);
  }

  public void drawStringC (double angle, String str, double x, double y,
    double dx) {
    drawStringC (angle, str, x, y, dx, 0);
  }

  public void drawStringC (double angle, String str, double x, double y,
    double dx, double dy) {
// /pl /it /bf  /tx /up /lw 
// angle            : [degree]
// x, y             :  user coordinates
// dx, dy           : [pt]
// offsetx, offsety :  ratio
    double rr = Math.toRadians (angle),
           xx = px(x) + dx, yy = py(y) - dy, xx0 = xx, yy0 = yy;
    Font font = g2.getFont();
    String fontname = font.getName ();
    int fontsize = font.getSize ();
    int h =  g2.getFontMetrics (font).getHeight (), status = 0;
    g2.rotate (-rr, xx0, yy0);
    for (int i = 0; i < str.length(); i++) {
      String s = str.substring(i, i+1);
      if (str.substring(i).startsWith ("/pl")) {
        setFont (fontname, Font.PLAIN, g2.getFont().getSize());
        i += 2;
      } else if (str.substring(i).startsWith ("/bf")) {
        setFont (fontname, Font.BOLD | g2.getFont().getStyle(),
          g2.getFont().getSize());
        i += 2;
      } else if (str.substring(i).startsWith ("/it")) {
        setFont (fontname, Font.ITALIC | g2.getFont().getStyle(),
          g2.getFont().getSize());
        i += 2;
      } else if (str.substring(i).startsWith ("/tx")) {
        setFont (fontname, g2.getFont().getStyle(), fontsize);
             if (status == +1) yy -= -0.4*h;
        else if (status == -1) yy -= +0.2*h;
        i += 2; status = 0;
      } else if (str.substring(i).startsWith ("/up")) {
        setFont (fontname, g2.getFont().getStyle(), (int) (0.8*fontsize));
             if (status ==  0) yy -= +0.4*h;
        else if (status == -1) yy -= +0.6*h;
        i += 2; status = +1;
      } else if (str.substring(i).startsWith ("/lw")) {
        setFont (fontname, g2.getFont().getStyle(), (int) (0.8*fontsize));
             if (status == +1) yy -= -0.6*h;
        else if (status ==  0) yy -= -0.2*h;
        i += 2; status = -1;
      } else {
        g2.drawString (s, (float) xx, (float) yy);
        xx += g2.getFontMetrics (g2.getFont()).stringWidth (s);
      }
    }
    g2.rotate (+rr, xx0, yy0);
    g2.setFont (font);
  }


  public void drawString (String str, double x, double y) {
    drawString (0, str, x, y, 0, 0, 0, 0);
  }

  public void drawString (String str, double x, double y, double dx) {
    drawString (0, str, x, y, dx, 0, 0, 0);
  }

  public void drawString (String str, double x, double y, double dx, double dy){
    drawString (0, str, x, y, dx, dy, 0, 0);
  }

  public void drawString (String str, double x, double y, double dx, double dy,
    double offsetx) {
    drawString (0, str, x, y, dx, dy, offsetx, 0);
  }

  public void drawString (String str, double x, double y, double dx, double dy,
    double offsetx, double offsety) {
    drawString (0, str, x, y, dx, dy, offsetx, offsety);
  }

  public void drawString (double angle, String str, double x, double y) {
    drawString (angle, str, x, y, 0, 0, 0, 0);
  }

  public void drawString (double angle, String str, double x, double y,
    double dx) {
    drawString (angle, str, x, y, dx, 0, 0, 0);
  }

  public void drawString (double angle, String str, double x, double y,
    double dx, double dy) {
    drawString (angle, str, x, y, dx, dy, 0, 0);
  }

  public void drawString (double angle, String str, double x, double y,
    double dx, double dy, double offsetx) {
    drawString (angle, str, x, y, dx, dy, offsetx, 0);
  }

  public void drawString (double angle, String str, double x, double y,
    double dx, double dy, double offsetx, double offsety) {
// angle            : [degree]
// x, y             :  user coordinates
// dx, dy           : [pt]
// offsetx, offsety :  ratio
    double rr = Math.toRadians (angle), xx = px(x) + dx, yy = py(y) - dy;
    if ((offsetx != 0) || (offsety != 0)) {
      TextLayout textLayout
        = new TextLayout (str, g2.getFont (), g2.getFontRenderContext ());
      double ww = offsetx*textLayout.getAdvance (),
             hh = offsety*textLayout.getAscent (),
             cc = Math.cos (rr), ss = Math.sin (rr);
      xx += cc*ww-ss*hh; yy -= ss*ww+cc*hh;
    }
    if (rr != 0) g2.rotate (-rr, xx, yy);
    g2.drawString (str, (float) xx, (float) yy);
    if (rr != 0) g2.rotate (+rr, xx, yy);
    if (rr != 0) {g2.rotate (-rr, xx, yy); g2.rotate (+rr, xx, yy);}
  }


  public void drawString (AttributedCharacterIterator str, double x, double y) {
    drawString (0, str, x, y, 0, 0, 0, 0);
  }

  public void drawString (AttributedCharacterIterator str, double x, double y,
    double dx) {
    drawString (0, str, x, y, dx, 0, 0, 0);
  }

  public void drawString (AttributedCharacterIterator str, double x, double y,
    double dx, double dy) {
    drawString (0, str, x, y, dx, dy, 0, 0);
  }

  public void drawString (AttributedCharacterIterator str, double x, double y,
    double dx, double dy, double offsetx) {
    drawString (0, str, x, y, dx, dy, offsetx, 0);
  }

  public void drawString (AttributedCharacterIterator str,
    double x, double y, double dx, double dy, double offsetx, double offsety) {
    drawString (0, str, x, y, dx, dy, offsetx, offsety);
  }

  public void drawString (double angle, AttributedCharacterIterator str,
    double x, double y) {
    drawString (angle, str, x, y, 0, 0, 0, 0);
  }

  public void drawString (double angle, AttributedCharacterIterator str,
    double x, double y, double dx) {
    drawString (angle, str, x, y, dx, 0, 0, 0);
  }

  public void drawString (double angle, AttributedCharacterIterator str,
    double x, double y, double dx, double dy) {
    drawString (angle, str, x, y, dx, dy, 0, 0);
  }

  public void drawString (double angle, AttributedCharacterIterator str,
    double x, double y, double dx, double dy, double offsetx) {
    drawString (angle, str, x, y, dx, dy, offsetx, 0);
  }

  public void drawString (double angle, AttributedCharacterIterator str,
    double x, double y, double dx, double dy, double offsetx, double offsety) {
// angle            : [degree]
// x, y             :  user coordinates
// dx, dy           : [pt]
// offsetx, offsety :  ratio
    double rr = Math.toRadians (angle), xx = px(x) + dx, yy = py(y) - dy;
    if ((offsetx != 0) || (offsety != 0)) {
      TextLayout textLayout = new TextLayout (str, g2.getFontRenderContext ());
      double ww = offsetx*textLayout.getAdvance (),
             hh = offsety*textLayout.getAscent (),
             cc = Math.cos (rr), ss = Math.sin (rr);
      xx += cc*ww-ss*hh; yy -= ss*ww+cc*hh;
    }
    if (rr != 0) g2.rotate (-rr, xx, yy);
    g2.drawString (str, (float) xx, (float) yy);
    if (rr != 0) g2.rotate (+rr, xx, yy);
    if (rr != 0) {g2.rotate (-rr, xx, yy); g2.rotate (+rr, xx, yy);}
  }


  public Shape getStringOutline (String str) {
    TextLayout textLayout
      = new TextLayout (str, g2.getFont (), g2.getFontRenderContext ());
    Shape outline = textLayout.getOutline (null);
    return outline;
  }

  public Shape getStringOutline (AttributedCharacterIterator str) {
    TextLayout textLayout = new TextLayout (str, g2.getFontRenderContext ());
    Shape outline = textLayout.getOutline (null);
    return outline;
  }


  public void drawShape (Shape s) {g2.draw (s);}

  public void drawShape (Shape s, double x, double y) {
    drawShape (0, s, x, y, 0, 0, 0, 0); 
  }

  public void drawShape (Shape s, double x, double y, double dx) {
    drawShape (0, s, x, y, dx, 0, 0, 0); 
  }

  public void drawShape (Shape s, double x, double y, double dx, double dy) {
    drawShape (0, s, x, y, dx, dy, 0, 0); 
  }

  public void drawShape (Shape s, double x, double y, double dx, double dy,
    double offsetx) {
    drawShape (0, s, x, y, dx, dy, offsetx, 0); 
  }

  public void drawShape (double angle, Shape s, double x, double y) {
    drawShape (angle, s, x, y, 0, 0, 0, 0); 
  }

  public void drawShape (double angle, Shape s, double x, double y, double dx) {
    drawShape (angle, s, x, y, dx, 0, 0, 0); 
  }

  public void drawShape (double angle, Shape s, double x, double y,
    double dx, double dy) {
    drawShape (angle, s, x, y, dx, dy, 0, 0); 
  }

  public void drawShape (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx) {
    drawShape (angle, s, x, y, dx, dy, offsetx, 0); 
  }

  public void drawShape (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx, double offsety) {
    double rr = Math.toRadians (angle), xx = px(x) + dx, yy = py(y) - dy;
    if ((offsetx != 0) || (offsety != 0)) {
      Rectangle2D bounds = s.getBounds2D ();
      double ww = offsetx*bounds.getWidth (), hh = offsety*bounds.getHeight (),
             cc = Math.cos (rr), ss = Math.sin (rr);
      xx += cc*ww-ss*hh; yy -= ss*ww+cc*hh;
    }
    AffineTransform at = new AffineTransform ();
    at.translate (xx, yy); if (rr != 0) at.rotate (-rr);
    g2.draw (at.createTransformedShape (s));
  }

  void fillShape (Shape s) {g2.fill (at.createTransformedShape (s));}

  public void fillShape (Shape s, double x, double y) {
    fillShape (0, s, x, y, 0, 0, 0, 0); 
  }

  public void fillShape (Shape s, double x, double y, double dx) {
    fillShape (0, s, x, y, dx, 0, 0, 0); 
  }

  public void fillShape (Shape s, double x, double y, double dx, double dy) {
    fillShape (0, s, x, y, dx, dy, 0, 0); 
  }

  public void fillShape (Shape s, double x, double y, double dx, double dy,
    double offsetx) {
    fillShape (0, s, x, y, dx, dy, offsetx, 0); 
  }

  public void fillShape (double angle, Shape s, double x, double y) {
    fillShape (angle, s, x, y, 0, 0, 0, 0); 
  }

  public void fillShape (double angle, Shape s, double x, double y, double dx) {
    fillShape (angle, s, x, y, dx, 0, 0, 0); 
  }

  public void fillShape (double angle, Shape s, double x, double y,
    double dx, double dy) {
    fillShape (angle, s, x, y, dx, dy, 0, 0); 
  }

  public void fillShape (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx) {
    fillShape (angle, s, x, y, dx, dy, offsetx, 0); 
  }

  public void fillShape (double angle, Shape s, double x, double y,
    double dx, double dy, double offsetx, double offsety) {
    double rr = Math.toRadians (angle), xx = px(x) + dx, yy = py(y) - dy;
    if ((offsetx != 0) || (offsety != 0)) {
      Rectangle2D bounds = s.getBounds2D ();
      double ww = offsetx*bounds.getWidth (), hh = offsety*bounds.getHeight (),
             cc = Math.cos (rr), ss = Math.sin (rr);
      xx += cc*ww-ss*hh; yy -= ss*ww+cc*hh;
    }
    AffineTransform at = new AffineTransform ();
    at.translate (xx, yy); if (rr != 0) at.rotate (-rr);
    g2.fill (at.createTransformedShape (s));
  }


  public void drawLine (double x0, double y0, double x1, double y1) {
    g2.draw (new Line2D.Double (px(x0), py(y0), px(x1), py(y1)));
  }

  public void drawLineD (double x0, double y0, double dx, double dy) {
// dx (positive leftward) and dy (positive upward) are scaled with pixels
    x0 = px(x0); y0 = py(y0);
    g2.draw (new Line2D.Double (x0, y0, x0+dx, y0-dy));
  }


  public void drawPath (double[] x, double[] y, double[] dx, double[] dy) {
    drawPath (x, y, dx, dy, 0, x.length);
  }

  public void drawPath (double[] x, double[] y, double[] dx, double[] dy,
    int start, int end) {
    Path2D.Double path = new Path2D.Double ();
    boolean normal, normalM = false;
    double xx, yy, xxM = Double.NaN, yyM = Double.NaN;
    for (int i = start; i < end; i++) {
      normal = !Double.isNaN ( x[i]) && !Double.isNaN ( y[i]) &&
               !Double.isNaN (dx[i]) && !Double.isNaN (dy[i]);
      if (normal) {
        xx = px(x[i])+dx[i]; yy = py(y[i])-dy[i];
        if (normalM) {
          if ((xx != xxM) || (yy != yyM)) path.lineTo (xx, yy);
        } else {
          path.moveTo (xx, yy);
        }
      } else {
        xx = Double.NaN; yy = Double.NaN;
      }
      normalM = normal; xxM = xx; yyM = yy;
    }
    g2.draw (path);
  }

  public void drawPath (double[] x, double[] y) {
    drawPath (x, y, 0, x.length);
  }

  public void drawPath (double[] x, double[] y, int start, int end) {
    Path2D.Double path = new Path2D.Double ();
    boolean normal, normalM = false;
    double xx, yy, xxM = Double.NaN, yyM = Double.NaN;
    for (int i = start; i < end; i++) {
      normal = !Double.isNaN (x[i]) && !Double.isNaN (y[i]);
      if (normal) {
        xx = px(x[i]); yy = py(y[i]);
        if (normalM) {
          if ((xx != xxM) || (yy != yyM)) path.lineTo (xx, yy);
        } else {
          path.moveTo (xx, yy);
        }
      } else {
        xx = Double.NaN; yy = Double.NaN;
      }
      normalM = normal; xxM = xx; yyM = yy;
    }
    g2.draw (path);
  }


  public void fillPath (double[] x, double[] y) {fillPath (x, y, 0, x.length);}

  public void fillPath (double[] x, double[] y, int start, int end) {
    Path2D.Double path = new Path2D.Double ();
    path.moveTo (px(x[start]), py(y[start]));
    for (int i = start+1; i < end; i++) path.lineTo (px(x[i]), py(y[i]));
    path.closePath();
    g2.fill (path);
  }

  public void fillPath (double[] x, double[] y, double[] dx, double[] dy) {
    fillPath (x, y, dx, dy, 0, x.length);
  }

  public void fillPath (double[] x, double[] y, double[] dx, double[] dy,
    int start, int end) {
    Path2D.Double path = new Path2D.Double ();
    path.moveTo (px(x[start])+dx[start], py(y[start])+dy[start]);
    for (int i = start+1; i < end; i++)
      path.lineTo (px(x[i])+dx[i], py(y[i])+dy[i]);
    path.closePath();
    g2.fill (path);
  }

  public void drawRectangle (double[] x0, double[] y0, double[] x1, double[] y1,
    int start, int end) {
    for (int i = start; i < end; i++) drawRectangle (x0[i],y0[i], x1[i],y1[i]);
  }

  public void drawRectangle (double[] x0,double[] y0, double[] x1,double[] y1) {
    drawRectangle (x0, y0, x1, y1, 0, x0.length);
  }

  public void drawRectangle (double x0, double y0, double x1, double y1) {
    double px0 = px (x0), py1 = py (y1);
    g2.draw (new Rectangle2D.Double (px0, py1, px(x1)-px0, py(y0)-py1));
  }

  public void fillRectangle (double[] x0, double[] y0, double[] x1, double[] y1,
    int start, int end) {
    for (int i = start; i < end; i++) fillRectangle (x0[i],y0[i], x1[i],y1[i]);
  }

  public void fillRectangle (double[] x0,double[] y0, double[] x1,double[] y1) {
    fillRectangle (x0, y0, x1, y1, 0, x0.length);
  }

  public void fillRectangle (double x0, double y0, double x1, double y1) {
    double px0 = px (x0), py1 = py (y1);
    g2.fill (new Rectangle2D.Double (px0, py1, px(x1)-px0, py(y0)-py1));
  }

  public void drawCircle (double[] x, double[] y, double r, int start, int end){
    for (int i = start; i < end; i++) drawCircle (x[i], y[i], r);
  }

  public void drawCircle (double[] x, double[] y, double r) {
    drawCircle (x, y, r, 0, x.length);
  }

  public void drawCircle (double x, double y, double r) {
    double d = r+r;
    if (!Double.isNaN (x) && !Double.isNaN (y))
      g2.draw (new Ellipse2D.Double (px(x)-r, py(y)-r, d, d));
  }

  public void fillCircle (double[] x, double[] y, double r, int start, int end){
    for (int i = start; i < end; i++) fillCircle (x[i], y[i], r);
  }

  public void fillCircle (double[] x, double[] y, double r) {
    fillCircle (x, y, r, 0, x.length);
  }

  public void fillCircle (double x, double y, double r) {
    double d = r+r;
    if (!Double.isNaN (x) && !Double.isNaN (y))
      g2.fill (new Ellipse2D.Double (px(x)-r, py(y)-r, d, d));
  }


  public void fillArc (double x, double y, double r0, double r1,
    double theta0, double theta1) {
    double d0 = r0+r0, d1 = r1+r1, extent = theta1-theta0;
    if (!Double.isNaN (x) && !Double.isNaN (y)) {
      Path2D.Double path = new Path2D.Double ();
      path.append (new Arc2D.Double (px(x)-r0, py(y)-r0, d0, d0,
        theta0, +extent, Arc2D.OPEN), true);
      path.append (new Arc2D.Double (px(x)-r1, py(y)-r1, d1, d1,
        theta1, -extent, Arc2D.OPEN), true);
      path.closePath();
      g2.fill (path);
    }
  }


  public void drawImage (BufferedImage img,
    double x0, double x1, double y0, double y1) {
    double px0 = px (x0), px1 = px (x1), py0 = py (y0), py1 = py (y1),
           fx = (px1-px0)/img.getWidth (), fy = (py1-py0)/img.getHeight ();
    g2.drawImage (img, new AffineTransform (fx, 0, 0, -fy ,px0, py1), null);
  }


  private void drawXTic (double x0, double x1, double y,
    double x, double ticLength) {
    if ((((x0-x) >= 0) && ((x-x1) >= 0)) || (((x0-x) <= 0) && ((x-x1) <= 0)))
      drawLineD (x, y, 0, ticLength);
  }

  private void drawXTic (boolean draw, int n, double x0, double x1, double y,
    double x, double dx, int[] nTic, float[] ticWidth, double[] ticLength) {
    if ((ticWidth[n] > 0) && (ticLength[n] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[n],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      if (draw) drawXTic (x0, x1, y, x, ticLength[n]);
    }
    if (n < nTic.length) {
      double dxx = dx/nTic[n];
      for (int i = 0; i < nTic[n]; i++)
        drawXTic (i!=0, n+1, x0, x1, y, x+i*dxx, dxx, nTic, ticWidth,ticLength);
    }
  }

  public void drawXAxis (float axisWidth, double x0, double x1, double y,
    double dx, int[] nTic, float[] ticWidth, double[] ticLength) {
//   int   [n]    nTic
//   float [n+1]  ticWidth
//   double[n+1]  ticLength
    if (axisWidth > 0) {
      g2.setStroke (new BasicStroke (axisWidth,
        BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
      drawLine (x0, y, x1, y);
    }
    for (double i = Math.floor(x0/dx); i <= Math.ceil(x1/dx); i++) {
      double x = i*dx;
      drawXTic (true, 0, x0, x1, y, x, dx, nTic, ticWidth, ticLength);
    }
  }

  private void drawYTic (double y0, double y1, double x,
    double y, double ticLength) {
    if ((((y0-y) >= 0) && ((y-y1) >= 0)) || (((y0-y) <= 0) && ((y-y1) <= 0)))
      drawLineD (x, y, ticLength, 0);
  }

  private void drawYTic (boolean draw, int n, double y0, double y1, double x,
    double y, double dy, int[] nTic, float[] ticWidth, double[] ticLength) {
    if ((ticWidth[n] > 0) && (ticLength[n] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[n],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      if (draw) drawYTic (y0, y1, x, y, ticLength[n]);
    }
    if (n < nTic.length) {
      double dyy = dy/nTic[n];
      for (int i = 0; i < nTic[n]; i++)
        drawYTic (i!=0, n+1, y0, y1, x, y+i*dyy, dyy, nTic, ticWidth,ticLength);
    }
  }

  public void drawYAxis (float axisWidth, double y0, double y1, double x,
    double dy, int[] nTic, float[] ticWidth, double[] ticLength) {
//   int   [n]    nTic
//   float [n+1]  ticWidth
//   double[n+1]  ticLength
    if (axisWidth > 0) {
      g2.setStroke (new BasicStroke (axisWidth,
        BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
      drawLine (x, y0, x, y1);
    }
    for (double i = Math.floor(y0/dy); i <= Math.ceil(y1/dy); i++) {
      double y = i*dy;
      drawYTic (true, 0, y0, y1, x, y, dy, nTic, ticWidth, ticLength);
    }
  }

  public void drawXLogAxis (float axisWidth, double x0, double x1, double y,
    float[] ticWidth, double[] ticLength) {
//   float [3]  ticWidth
//   double[3]  ticLength
    double log2 = Math.log10 (2), log5 = Math.log10 (5),
           log3 = Math.log10 (3), log4 = Math.log10 (4),
           log6 = Math.log10 (6), log7 = Math.log10 (7),
           log8 = Math.log10 (8), log9 = Math.log10 (9);
    if (axisWidth > 0) {
      g2.setStroke (new BasicStroke (axisWidth,
        BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
      drawLine (x0, y, x1, y);
    }
    if (ticWidth.length == 0) return;
    if ((ticWidth[0] > 0) && (ticLength[0] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[0],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double x = Math.floor(x0); x <= Math.ceil(x1); x++)
        drawXTic (x0, x1, y, x, ticLength[0]);
    }
    if (ticWidth.length == 1) return;
    if ((ticWidth[1] > 0) && (ticLength[1] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[1],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double x = Math.floor(x0); x <= Math.ceil(x1); x++) {
        drawXTic (x0, x1, y, x+log2, ticLength[1]);
        drawXTic (x0, x1, y, x+log5, ticLength[1]);
      }
    }
    if (ticWidth.length == 2) return;
    if ((ticWidth[2] > 0) && (ticLength[2] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[2],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double x = Math.floor(x0); x <= Math.ceil(x1); x++) {
        double x3 = x+log3, x4 = x+log4,
               x6 = x+log6, x7 = x+log7, x8 = x+log8, x9 = x+log9;
        drawXTic (x0, x1, y, x+log3, ticLength[2]);
        drawXTic (x0, x1, y, x+log4, ticLength[2]);
        drawXTic (x0, x1, y, x+log6, ticLength[2]);
        drawXTic (x0, x1, y, x+log7, ticLength[2]);
        drawXTic (x0, x1, y, x+log8, ticLength[2]);
        drawXTic (x0, x1, y, x+log9, ticLength[2]);
      }
    }
  }

  public void drawYLogAxis (float axisWidth, double y0, double y1, double x,
    float[] ticWidth, double[] ticLength) {
//   float [3]  ticWidth
//   double[3]  ticLength
    double log2 = Math.log10 (2), log5 = Math.log10 (5),
           log3 = Math.log10 (3), log4 = Math.log10 (4),
           log6 = Math.log10 (6), log7 = Math.log10 (7),
           log8 = Math.log10 (8), log9 = Math.log10 (9);
    if (axisWidth > 0) {
      g2.setStroke (new BasicStroke (axisWidth,
        BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
      drawLine (x, y0, x, y1);
    }
    if (ticWidth.length == 0) return;
    if ((ticWidth[0] > 0) && (ticLength[0] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[0],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double y = Math.floor(y0); y <= Math.ceil(y1); y++)
        drawYTic (y0, y1, x, y, ticLength[0]);
    }
    if (ticWidth.length == 1) return;
    if ((ticWidth[1] > 0) && (ticLength[1] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[1],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double y = Math.floor(y0); y <= Math.ceil(y1); y++) {
        drawYTic (y0, y1, x, y+log2, ticLength[1]);
        drawYTic (y0, y1, x, y+log5, ticLength[1]);
      }
    }
    if (ticWidth.length == 2) return;
      if ((ticWidth[2] > 0) && (ticLength[2] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[2],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
      for (double y = Math.floor(y0); y <= Math.ceil(y1); y++) {
        drawYTic (y0, y1, x, y+log3, ticLength[2]);
        drawYTic (y0, y1, x, y+log4, ticLength[2]);
        drawYTic (y0, y1, x, y+log6, ticLength[2]);
        drawYTic (y0, y1, x, y+log7, ticLength[2]);
        drawYTic (y0, y1, x, y+log8, ticLength[2]);
        drawYTic (y0, y1, x, y+log9, ticLength[2]);
      }
    }
  }

  public void drawMonthXAxis (float axisWidth,
    double julianDay0, double julianDay1,
    double y, float[] ticWidth, double[] ticLength) {
//   float [5]  ticWidth   {year, month, 10days, 5days, 1day}
//                         {1day, 6hours, 3hours, 1hour, 10min}
//   double[5]  ticLength
    if (axisWidth > 0) {
      g2.setStroke (new BasicStroke (axisWidth,
        BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
      drawLine (julianDay0, y, julianDay1, y);
    }
    if (ticWidth.length == 0) return;
    if ((ticWidth[0] > 0) && (ticLength[0] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[0],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        int[] ymd = ADate.JulianDayToYMD (jd);
        if ((ymd[1] == 1) && (ymd[2] == 1))
          drawXTic (x0, x1, y, jd, ticLength[0]);
      }
    } else {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        drawXTic (x0, x1, y, jd, ticLength[0]);
      }
    }}
    if (ticWidth.length == 1) return;
    if ((ticWidth[1] > 0) && (ticLength[1] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[1],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        int[] ymd = ADate.JulianDayToYMD (jd);
        if ((ymd[1] != 1) && (ymd[2] == 1))
          drawXTic (x0, x1, y, jd, ticLength[1]);
      }
    } else {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
      for (int h = 6; h < 24; h+=6) {
        drawXTic (x0, x1, y, jd+h/24.0, ticLength[1]);
      }}
    }}
    if (ticWidth.length == 2) return;
    if ((ticWidth[2] > 0) && (ticLength[2] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[2],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        int[] ymd = ADate.JulianDayToYMD (jd);
        if ((ymd[2] % 10) == 0) drawXTic (x0, x1, y, jd, ticLength[2]);
      }
    } else {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
      for (int h = 3; h < 24; h+=6) {
        drawXTic (x0, x1, y, jd+h/24.0, ticLength[2]);
      }}
    }}
    if (ticWidth.length == 3) return;
    if ((ticWidth[3] > 0) && (ticLength[3] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[3],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        int[] ymd = ADate.JulianDayToYMD (jd);
        if (((ymd[2] % 10) != 0) && ((ymd[2] % 5) == 0))
          drawXTic (x0, x1, y, jd, ticLength[3]);
      }
    } else {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
      for (int h = 1; h < 24; h++) {
        if ((h % 3) != 0) drawXTic (x0, x1, y, jd+h/24.0, ticLength[3]);
      }}
    }}
    if (ticWidth.length == 4) return;
    if ((ticWidth[4] > 0) && (ticLength[4] != 0)) {
      g2.setStroke (new BasicStroke (ticWidth[4],
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
        int[] ymd = ADate.JulianDayToYMD (jd);
        if ((ymd[2] % 5) != 0) drawXTic (x0, x1, y, jd, ticLength[4]);
      }
    } else {
      for (int jd =  (int) Math.floor (julianDay0);
               jd <= (int) Math.ceil  (julianDay1); jd++) {
      for (int h = 0; h < 24; h++) {
      for (int m = 10; m < 60; m+=10) {
        drawXTic (x0, x1, y, jd+(h+m/60.0)/24.0, ticLength[4]);
      }}}
    }}
  }
/*
    g2.setStroke (new BasicStroke (ticWidth,
      BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
    if (julianDay1-julianDay0 > 7) {
      for (int i = (int) Math.floor (julianDay0);
              i <= (int) Math.ceil  (julianDay1); i++) {
        int[] ymd = ADate.JulianDayToYMD (i);
        double tic = (ymd[2] == 1) ?  ((ymd[1] == 1) ? ticYear: ticMonth):
                    ((ymd[2] % 10 == 0) ? tic0len:
                    ((ymd[2] % 5 == 0) ? tic1len: tic2len));
        if ((x0-i)*(i-x1) >= 0) drawLineD (i, y, 0, tic);
      }
    } else {
      for (int i = (int) Math.floor (julianDay0);
              i <= (int) Math.ceil  (julianDay1); i++) {
        for (int h = 0; h < 24; h++) {
          double x = i+h/24.0;
          if ((julianDay0 <= x) && (x <= julianDay1)) {
            double tic = (h == 0) ? tic0len: ((h % 6 == 0) ? tic1len: tic2len);
            if ((x0-x)*(x-x1) >= 0) drawLineD (x, y, 0, tic);
          }
        }
      }
    }
  }
*/


///////////////////



  void drawArrow (double x0, double y0, double x1, double y1, double asize) {
// asixe is scaled with pixels
    x0 = px(x0); y0 = py(y0); x1 = px(x1); y1 = py(y1);
    double dx = x1-x0, dy = y1-y0,
      c = dx/Math.sqrt(dx*dx+dy*dy), s = dy/Math.sqrt(dx*dx+dy*dy);
    double[] x = {x1, x1, x1, x1, x1}, y  = {y1, y1, y1, y1, y1};
//    for (int i = 0; i < x.length; i++) {
//      x[i] += asize*0.2*c; y[i] += asize*0.2*s;}
    x[1] += -asize*(1.2*c+0.3*s); y[1] += -asize*(1.2*s-0.3*c);
    x[2] += -asize*c;             y[2] += -asize*s;
    x[3] += -asize*(1.2*c-0.3*s); y[3] += -asize*(1.2*s+0.3*c);
    g2.draw (new Line2D.Double (x0, y0, x[2], y[2]));
    GeneralPath polygon = new GeneralPath ();
    polygon.moveTo ((float) x[0], (float) y[0]);
    for (int i = 1; i < x.length; i++)
      polygon.lineTo ((float) x[i], (float) y[i]);
    polygon.closePath();
    g2.fill (polygon);
}

  void contour (double level,
    double[][] data, double[] x, double[] y, double pad) {
// data [x.length][y.length]
    int nx = x.length, ny = y.length;
    if (nx < 2) {
      System.out.format ("nx %d is less than 2", nx);
      return;
    }
    if (ny < 2) {
      System.out.format ("ny %d is less than 2", ny);
      return;
    }
    for (int i = 1; i < nx; i++) {int im = i-1;
    for (int j = 1; j < ny; j++) {int jm = j-1;
      boolean px0, px1, py0, py1;
      double  rx0 = 0, rx1 = 0, ry0 = 0, ry1 = 0;
      if ((data[im][jm] == pad) || (data[im][j ] == pad)
       || (data[i ][jm] == pad) || (data[i ][j ] == pad)) continue;
      if (px0 = ((data[im][jm]!=data[i ][jm]) &&
                ((data[i ][jm]-level)*(level-data[im][jm]) >= 0)))
          rx0 = (level-data[im][jm])/(data[i ][jm]-data[im][jm]);
      if (px1 = ((data[im][j ]!=data[i ][j ]) &&
                ((data[i ][j ]-level)*(level-data[im][j ]) >= 0)))
          rx1 = (level-data[im][j ])/(data[i ][j ]-data[im][j ]);
      if (py0 = ((data[im][jm]!=data[im][j ]) &&
                ((data[im][j ]-level)*(level-data[im][jm]) >= 0)))
          ry0 = (level-data[im][jm])/(data[im][j ]-data[im][jm]);
      if (py1 = ((data[i ][jm]!=data[i ][j ]) &&
                ((data[i ][j ]-level)*(level-data[i ][jm]) >= 0)))
          ry1 = (level-data[i ][jm])/(data[i ][j ]-data[i ][jm]);
      if (px0 && px1 && py0 && py1) {
      if (((rx0<=0.5)&&(ry0 <= 0.5)) || ((rx1>0.5)&&(ry1>0.5))) {
        drawLine (x[im]+rx0*(x[i]-x[im]), y[jm], x[im], y[jm]+ry0*(y[j]-y[jm]));
        drawLine (x[im]+rx1*(x[i]-x[im]), y[j ], x[i ], y[jm]+ry1*(y[j]-y[jm]));
      } else {
        drawLine (x[im]+rx0*(x[i]-x[im]), y[jm], x[i ], y[jm]+ry1*(y[j]-y[jm]));
        drawLine (x[im]+rx1*(x[i]-x[im]), y[j ], x[im], y[jm]+ry0*(y[j]-y[jm]));
      }} else if (px0 && px1) {
        drawLine (x[im]+rx0*(x[i]-x[im]), y[jm], x[im]+rx1*(x[i]-x[im]), y[j]);
      } else if (px0 && py0) {
        drawLine (x[im]+rx0*(x[i]-x[im]), y[jm], x[im], y[jm]+ry0*(y[j]-y[jm]));
      } else if (px0 && py1) {
        drawLine (x[im]+rx0*(x[i]-x[im]), y[jm], x[i ], y[jm]+ry1*(y[j]-y[jm]));
      } else if (px1 && py0) {
        drawLine (x[im]+rx1*(x[i]-x[im]), y[j ], x[im], y[jm]+ry0*(y[j]-y[jm]));
      } else if (px1 && py1) {
        drawLine (x[im]+rx1*(x[i]-x[im]), y[j ], x[i ], y[jm]+ry1*(y[j]-y[jm]));
      } else if (py0 && py1) {
        drawLine (x[im], y[jm]+ry0*(y[j]-y[jm]), x[i ], y[jm]+ry1*(y[j]-y[jm]));
      }
    }}
  }


  void drawArc (double x0, double y0, double r, double start, double extent) {
// start, extent in Degrees
    double d = r+r;
    g2.draw (new Arc2D.Double (px(x0)-r, py(y0)-r, d, d,
      start, extent, Arc2D.OPEN));
  }

   X   �#6�P�>c�Y���>�h}e��>���>7���c�>�P萨�>o�J��>�/2�>�8S�v�>C�vu��>A�  p      KT6��'4�%cY�%cY?%cY?~�?BI�;��    ߓ8�Bm6�W�W?W?�D?fM�;i�     �:�]�8��T��T?�T?Vx?�O�;�<�    ��<���:��cR��cR?�cR?�?SQ�;�P�    �%?�=�I P�I P?I P?�?�R�;�N�    �FA��8?��M��M?�M?]�?T�;7�    F`C�9XA�%K�%K?%K?�l?�V�;�	�    �qE�pC�`�H�`�H?`�H?�3?U[�;���    s{G��E�/F�/F?/F?c�?Zb�;'n�    }I��G�A�C�A�C?A�C?_�?�l�;8 �S�  X   �}�. �>{ ��D�>�᠉�>�eZ��>O)�>�L�W�>�Mp���>#�>��>����%�>[5۰j�>A�  p      mvK�	�I�
A�
A?
A?�?�z�;�|�    �gM��K���>���>?��>?`y?L��;z��    �PO�coM���;���;?��;?��?1��;�6�    1Q��VO�X9�X9?X9?�?ë;nt�    <	S��5Q�?�6�?�6??�6?YO?h�;'��    ��T��S��
4��
4?�
4?m?�;D��    ןV���T�)[1�)[1?)[1?dv?�F�;��    /^X��V��.��.?�.?[k?Ȃ�;`��    �Z��[X�m�+�m�+?m�+?6L?%ȓ;�s�    ��[��Z�X+)�X+)?X+)?,?]�;[7�S�  X   ���i��>�z"#��>/F�8�>˿i�}�>gb�N��>��>����K�>;J�y��>��3��>s�?��>A�  p      �d]���[�on.lineTo ((float) x[i], (float) y[i]);
    polygon.closePath();
    g2.fill (polygon);
  }



  void drawQuadCurve(double x1, double y1, double ctrlx, double ctrly,
    double x2, double y2) {
    g2.draw (new QuadCurve2D.Double (px(x1), py(y1), px(ctrlx), py(ctrly),
      px(x2), py(y2)));
  }


}
