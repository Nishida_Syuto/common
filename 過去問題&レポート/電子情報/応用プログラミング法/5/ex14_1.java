class ex14_1 {

/* exp(x) by Taylor series */

  static double exp (double x){
    boolean negative = x < 0;
    if (negative) x = -x;
/*
    double ret = 1, term = 1;
    for (int i = 1; i < 10; i++){
      term *= x/i;
      ret += term;
      System.out.format ("i=%d ret=%-20s term = %-20s\n",i,Double.toString (ret), Double.toString (term));

    }
*/

    int i=0;
    double ret = 1, term = 1, eps = 1e-8;
    do {
      ++i;
      term *= x/i;
      ret += term;
      System.out.format(" ret = %-24s term = %-24s\n",Double.toString (ret), Double.toString (term));

    }while (term > eps*ret);


    return(negative) ? 1/ret: ret;
  }

  public static void main (String args[]){
    if (args.length>0){
      try{
        double x =Double.parseDouble (args[0]), y=exp(x);
        System.out.println(
          "sqrt("+Double.toString(x)+")="+Double.toString(y));
      }catch (NumberFormatException e){
        System.err.println("\""+args[0]+"\" is not a number.");
        System.exit(1);
      }
    }else{
      System.out.println("Usage: java ex14 (number)");
    }
  }
}
