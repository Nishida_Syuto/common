import java.util.Arrays;

class ex11{

    static String addZero (String s,int nbits){
	char[]a=new char[nbits -s.length()];
	Arrays.fill(a,'0');
	return new String(a)+s;
    }

    static void printInt(int i){
	System.out.format("i=%d\n",i);
	System.out.format("hexadecimal:%8s\n",Integer.toHexString(i));
	System.out.format("octal:%11s\n",Integer.toOctalString(i));
       	System.out.format("binary:%32s\n",Integer.toBinaryString(i));
	//	System.out.format("binary:%32s\n",addZero(Integer.toBinaryString(i),32));
    }

    public static void main (String args[]){
	printInt  (1);
	printInt  (2);
	printInt  (3);
	//	printInt  (0);
	//	printInt  (-1);
	//	printInt  (-2);
	//	printInt  (-3);
	//	printInt  (Integer.MAX_VALUE);
	//	printInt  (Integer.MIN_VALUE);

	/*
	  System.out.println();
	  System.out.format("#bits of byte : %2d\n",Byte.SIZE);
	  System.out.format("#bits of short : %2d\n",Short.SIZE);
	  System.out.format("#bits of int : %2d\n",Int.SIZE);
	  System.out.format("#bits of long : %2d\n",Long.SIZE);
	*/
    }
}