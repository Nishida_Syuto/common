import java.util.Arrays;

class ex12 {
    static String addZero (String s,int nbits){
	char[] a = new char [nbits - s.length ()];
	Arrays.fill(a,'0');
	return new String (a) + s;
    }

    static void printDouble (double x){
	System.out.format ("x= %s\n",Double.toString (x));
	System.out.format (" hex : %s\n",Double.toHexString (x));
	System.out.format ("     : %016x\n",Double.doubleToRawLongBits (x));
	System.out.format (" bit : %64s\n",addZero (Long.toBinaryString (Double.doubleToRawLongBits (x)),64));
    }

    public static void main (String args[]){
	printDouble (1);
	printDouble (2);
	printDouble (3);
                printDouble (5);
		printDouble (6);
		printDouble (7);
                printDouble (0);
		printDouble (-1);
		printDouble (-2);
		printDouble (-3);
		printDouble (0.1);
	printDouble (0.2);
	printDouble (0.3);
	printDouble (0.4);
		printDouble (Double.MAX_VALUE);
		printDouble (Double.MIN_NORMAL);
		printDouble (Double.MIN_VALUE);
		printDouble (Double.POSITIVE_INFINITY);
		printDouble (Double.NEGATIVE_INFINITY);
		printDouble (Double.NaN);
		printDouble (Math.PI);
		printDouble (Math.E);

	
	  System.out.println();
	  System.out.format("#bits of double : %2d\n", Double.SIZE);
	  System.out.format("max exponent of finite double     : %5d\n",Double.MAX_EXPONENT);
	  System.out.format("min exponent of normalized double : %5d\n",Double.MIN_EXPONENT);
	

	/*
	  System.out.println();
	  printDouble(0.1);
	  printDouble(0.4 - 0.3);
	*/

    }
}
