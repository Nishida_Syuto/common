import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.util.*;

public class ex22 {

  static class HSBSlider extends JSlider implements ChangeListener {
    String title;
    float value;

    HSBSlider (String title, float value) {
      this.title = title;
      this.value = value;
      setBorder (BorderFactory.createTitledBorder (
        BorderFactory.createEtchedBorder (),
        String.format ("[%s = %.3f]", title, value),
        TitledBorder.LEFT, TitledBorder.TOP));
      setMinimum (0); setMaximum (1000); setValue ((int) (1000*value));
      setMajorTickSpacing (100); setMinorTickSpacing (20);
      setLabelTable (makeLabels ());
      setPaintTicks (true);
      setPaintLabels (true);
      addChangeListener (this);
      Dimension size = getPreferredSize ();
      size.setSize (420, 60);
      setMinimumSize (size); setPreferredSize (size); setMaximumSize (size);
    }

    private Hashtable <Integer, JComponent> makeLabels () {
      Hashtable <Integer, JComponent>
        t = new Hashtable <Integer, JComponent> ();
        t.put (new Integer (   0), new JLabel ("0.0"));
        t.put (new Integer ( 500), new JLabel ("0.5"));
        t.put (new Integer (1000), new JLabel ("1.0"));
      return t;
    }

    public void stateChanged (ChangeEvent ev) {
      value  = getValue ()/1000f;
      setBorder (BorderFactory.createTitledBorder (
        BorderFactory.createEtchedBorder (),
        String.format ("[%s = %.3f]", title, value),
        TitledBorder.LEFT, TitledBorder.TOP));
      getRootPane ().repaint ();
    }
 
    void  setIndicator (float value) {
      this.value = value;
      setValue ((int) (1000*value));
    }
    float getIndicator () {return value;}
  }

  static class Ex22 extends JPanel implements MouseInputListener {
    int radius = 200, x0 = 100+radius, y0 = 130+radius;
    float hue = Float.NaN, saturation = Float.NaN, brightness = Float.NaN;
    boolean locked = false;
    HSBSlider hSlider, sSlider, bSlider;

    Ex22 () {
      hSlider = new HSBSlider ("Hue          ", 0.0f);
      sSlider = new HSBSlider ("Saturation", 1.0f);
      bSlider = new HSBSlider ("Brightness", 0.85f);
      setLayout (new BoxLayout (this, BoxLayout.Y_AXIS));
      add (Box.createVerticalStrut (15));
      add (hSlider);
      add (sSlider);
      add (bSlider);
      add (Box.createRigidArea (new Dimension (
        (int) APlot.A4.getWidth (), (int) (0.85*APlot.A4.getHeight ()))));
      add (Box.createVerticalStrut (-100));
      addMouseListener (this);
      addMouseMotionListener (this);
      setBackground (Color.white);
    }

    public void paintComponent (Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      hue        = hSlider.getIndicator ();
      saturation = sSlider.getIndicator ();
      brightness = bSlider.getIndicator ();
      int rstep = radius/10;
      double pt = 25.4/72;
      a.setCoordinate ((x0-radius)*pt, (y0-radius)*pt, 2*radius*pt, 2*radius*pt,
        -1, 1, -1, 1);
      for (int r = rstep; r <= radius; r += rstep) {
        int astep = (r < 100) ? 10: 5;
      for (int angle  = 0; angle <= 360; angle += astep) {
        float h = angle/360f, s = r/200f;
        g2.setColor (Color.getHSBColor (h, s, brightness));
        a.fillArc (0, 0, r-rstep+2, r, angle+0.5, angle+astep-0.5);
      }}

      g2.setColor (Color.darkGray);
      a.setFont ("SansSerif", 14);
      a.drawString ("HSB Color Chart", -1.08, 1.08);
      g2.setStroke (new BasicStroke (1f,
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
      a.setFont ("SansSerif", 12);
      a.drawCircle (0, 0, 1.01*radius);
      for (int i = 0; i < 360; i += 10) {
        double r = Math.toRadians (i), c = Math.cos (r), s = Math.sin (r),
          len = ((i % 30) == 0) ? 9: 7;
        a.drawLineD (1.01*c, 1.01*s, len*c, len*s);
        if ((i % 90) == 0) a.drawString (90.01+i,
          String.format ("Hue:%5.2f", i/360.0), c, s, 12*c, 12*s, -0.5, -1);
      }
      a.drawXAxis (1f, 0, 1, -1.2, 1, new int[] {2, 5},
        new float[] {1f, 1f, 1f}, new double[] {-10, -8, -6});
      a.drawString ("Saturation: ", 0, -1.2, -10, -20, -1);
      a.drawString ("0.0", 0.0, -1.2, 0, -20, -0.5);
      a.drawString ("0.5", 0.5, -1.2, 0, -20, -0.5);
      a.drawString ("1.0", 1.0, -1.2, 0, -20, -0.5);
/*
      if (!Float.isNaN (hue)) {
        a.drawString ("Hue:", -1, -1.2, 0, 30);
        a.drawString (String.format ("%6.3f", hue), -0.5, -1.2, 0, 30, -1);
      }
      if (!Float.isNaN (saturation)) {
        a.drawString ("Saturation:", -1, -1.2, 0, 15);
        a.drawString (String.format ("%6.3f", saturation), -0.5, -1.2, 0,15,-1);
      }
      if (!Float.isNaN (brightness)) {
        a.drawString ("Brightness:", -1, -1.2 );
        a.drawString (String.format ("%6.3f", brightness), -0.5, -1.2, 0, 0,-1);
      }
*/
      if ((!Float.isNaN (hue)) && (!Float.isNaN (saturation))
        && (!Float.isNaN (brightness))) {
        g2.setColor (Color.getHSBColor (hue, saturation, brightness));
        a.fillRectangle (-1.05, -1.5, -0.45, -1.02);
      }
      g2.setColor ((locked) ? Color.black: Color.white);
      a.drawString ("locked", -0.45, -1.5, 0, 0, -1, -1);
    }

    void renewColor (Point p) {
      double x = (p.x-x0)/(double) radius,
        y = (getHeight()-p.y-y0)/(double) radius,
        r = Math.hypot (x, y), theta = Math.atan2 (y, x);
      if (r <= 1) {
        hue = (float) (0.5*theta/Math.PI); if (hue < 0) hue += 1;
        saturation = (float) r;
        hSlider.setIndicator (hue);
        sSlider.setIndicator (saturation);
      }
    }

    public void mouseMoved (MouseEvent e) {
      if (!locked) renewColor (e.getPoint ());
      repaint ();
    }

    public void mouseClicked (MouseEvent e) {
      locked = !locked;
      if (!locked) renewColor (e.getPoint ());
      repaint ();
    }

    public void mouseDragged  (MouseEvent e) {}
    public void mouseEntered  (MouseEvent e) {}
    public void mouseExited   (MouseEvent e) {}
    public void mousePressed  (MouseEvent e) {}
    public void mouseReleased (MouseEvent e) {}
  }

  private static void createPanel () {
    JFrame f = new JFrame ("ex22");
    Ex22 p = new Ex22 ();
    p.setOpaque (true);
    f.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    f.setContentPane (p);
    f.pack ();
    f.setVisible (true);
  }

  public static void main (String[] args)  {
    SwingUtilities.invokeLater (new Runnable () {
      public void run () {
        createPanel ();
      }
    });
  }
}
