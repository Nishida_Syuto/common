import java.awt.*;
import javax.swing.*;

public class ex23 {
    static class Ex23 extends JPanel {
	String myname;
	int x0=30,y0=30,xlen=100,ylen=200;

	class ColorScheme {
	    String name;
	    double[][] color;
	    ColorScheme(String name){this.name=name;}
	}
	ColorScheme ki,ao,aka,midori;

	Ex23(){
	    myname="T150173 Ryota Tanimoto";
	    ki = new ColorScheme("ki");
	    ao = new ColorScheme("ao");
	    aka = new ColorScheme("aka");
	    midori = new ColorScheme("midori");
	   
	    ao.color=new double[][]{
		{0.00,0.8,1.0},
		{0.16,0.8,1.0},
		{0.25,0.8,1.0},
		{0.65,0.8,1.0}};

	    setPreferredSize(APlot.A4);
	    setBackground(Color.white);
	    //drawn=ki;
	    drawn=ao;
	    //drawn=aka;
	    // drawn=midori;
	}

	public void paintComponet(Grahics g){
	    super.paintComponent(g);
	    Graphics2D g2 = (Graphics2D) g;
	    APlot a = new APlot (this,g2);

	    a.setCoordinate(x0,y0,xlen,ylen,0,1,0,1);
	    g2.setColor(Color.black);
	    a.setFont("Serif",20);
	    a.drawString(myname,0,1,2,40);
	    a.drawString(drawn.name,0,1,2,10);
	    a.setFont("Sansserif",11);
	    a.drawString("Huw / Saturation / Brightness",1,1,2,10);

	    double vsize = 1.0/draw.color.length;
	    for(int i=0;i<draw.color.lebgth;i++){
		g2.setColor(Color.black);
		a.drawString(String.format("%7.4f / %7.4f / %7.4f",drawn.color[i][0],drawn.color[i][1],drawn.color[i][2]),1,1-i*vsize,4,-10);
		g2.setColor(Color.getHSBColor((float)drawn.color[i][0],(float)drawn.color[i][1],(float)drawn.color[i][2]));
		a.fillRectangle(0,1-(i+1)*vsize,1,1-i*vsize);
	    }
	}
    }

    public static void main(String argc[]){
	APage p =new APage (new Ex23(),"ex23.ps");
    }
}