import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Printable;
import java.awt.print.PageFormat;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.StreamPrintService;
import javax.print.StreamPrintServiceFactory;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.ResolutionSyntax;
import javax.print.attribute.standard.*;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class APage implements Printable {
  private boolean paint;
  private JFrame f;
  private JPanel p;

  APage (JPanel p, String filename) {
    this (true, p, filename, MediaSizeName.ISO_A4, false);
  }

  APage (JPanel p, String filename, MediaSizeName msn) {
    this (true, p, filename, msn, false);
  }

  APage (JPanel p, String filename, boolean landscape) {
    this (true, p, filename, MediaSizeName.ISO_A4, landscape);
  }

  APage (JPanel p, String filename, MediaSizeName msn, boolean landscape) {
    this (true, p, filename, msn, landscape);
  }

  APage (boolean paint, JPanel p, String filename) {
    this (paint, p, filename, MediaSizeName.ISO_A4, false);
  }

  APage (boolean paint, JPanel p, String filename, MediaSizeName msn) {
    this (paint, p, filename, msn, false);
  }

  APage (boolean paint, JPanel p, String filename, boolean landscape) {
    this (paint, p, filename, MediaSizeName.ISO_A4, landscape);
  }

  APage (boolean paint, JPanel p, String filename, MediaSizeName msn,
    boolean landscape) {
    float msx = MediaSize.getMediaSizeForName (msn).getX (MediaSize.MM),
          msy = MediaSize.getMediaSizeForName (msn).getY (MediaSize.MM);
    this.paint = paint;
    this.p = p; 
    f = new JFrame (filename.substring (0, filename.length ()-3));
    f.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
    String psMimeType = DocFlavor.BYTE_ARRAY.POSTSCRIPT.getMimeType ();
    StreamPrintServiceFactory[] factories =
    StreamPrintServiceFactory.lookupStreamPrintServiceFactories
      (flavor, psMimeType);
    if (factories.length == 0) {
      System.err.println ("ERROR in APage : No suitable factories");
      System.exit(0);
    }
    try {
      FileOutputStream fos = new FileOutputStream (filename);
      StreamPrintService sps = factories[0].getPrintService (fos);
      DocPrintJob pj = sps.createPrintJob ();
      PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet ();
//aset.add (new PrinterResolution (600, 600, ResolutionSyntax.DPI));
      aset.add (msn);
      if (landscape) aset.add (OrientationRequested.LANDSCAPE);
      aset.add (new MediaPrintableArea (0, 0, msx, msy, MediaSize.MM));
      Doc doc = new SimpleDoc (this, flavor, null);
      pj.print (doc, aset);
      fos.close ();
    } catch (PrintException pe) { 
      System.err.println (pe);
    } catch (IOException ie) { 
      System.err.println (ie);
    }
  }

  public int print (Graphics g, PageFormat pf, int pageIndex) {
    if (pageIndex == 0) {
      p.setOpaque (true);
      f.setContentPane (p);
      f.pack ();
      if (paint) {
        f.setVisible (true);
        Dimension d = f.getPreferredSize ();
        f.setSize (new Dimension (d.width+15, d.height));
      }
      Graphics2D g2 = (Graphics2D) g;
//    g2.translate (72, 72);
      p.printAll (g);
      return Printable.PAGE_EXISTS;
    } else {
      return Printable.NO_SUCH_PAGE;
    }
  }

}
