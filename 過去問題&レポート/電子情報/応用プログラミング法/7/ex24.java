import java.awt.*;
import java.util.*;
import javax.swing.*;

public class ex24 {
  static class Ex24 extends JPanel {
    String myname = "T150000  MyName";
    int x0 = 30, y0 = 30, xlen = 100, ylen = 200;
    AColorScheme cs;

    Ex24 () {
      cs = new AColorScheme ();
      cs.put (200.0, 0.10, 0.35, 1);
      cs.put (110.0, 0.17, 0.35, 1);
      cs.put ( 94.0, 0.33, 0.35, 1);
      cs.put ( 86.0, 0.53, 0.40, 1);
      cs.put ( 84.0, 0.73, 0.40, 1);

      setPreferredSize (APlot.A4);
      setBackground (Color.white);
    }

    public void paintComponent (Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);
      double h0 = cs.color.firstKey (), h1 = cs.color.lastKey ();

      a.setCoordinate (x0, y0, xlen, ylen, 0, 1, h0, h1);
      g2.setColor (Color.black);
      a.setFont ("SansSerif", 11);
      a.drawString ("Hue / Saturation / Brightness", 1, 1, 8, 12);

      int i = 0;
      double vsize = 1.0/cs.color.size (), lastHeight, hstep = 0.5;
      Double height = cs.color.firstKey ();
      do {
        float[] hsb =  cs.get (height);
        g2.setColor (Color.black);
        a.drawString (String.format ("%4.0f", height), 0, height, -4, -4, -1);
        a.drawString (String.format ("%7.4f  / %7.4f  / %7.4f",
          hsb[0], hsb[1], hsb[2]), 1, height, 4, -4);
        g2.setColor (Color.getHSBColor (hsb[0], hsb[1], hsb[2]));
        i++; lastHeight = height;
      } while ((height = cs.color.higherKey (height)) != null);

      for (double h = Math.floor (h0); h <= Math.ceil (h1); h += hstep) {
        float[] hsb = cs.get (h);
        g2.setColor (Color.getHSBColor (hsb[0], hsb[1], hsb[2]));
        a.fillRectangle (0, h-0.5*hstep, 1, h+0.5*hstep);
      }
    }
  }

  public static void main (String args[]) {
    APage p = new APage (true, new Ex24 (), "ex24.ps");
    //System.exit(-1);
  }
}

