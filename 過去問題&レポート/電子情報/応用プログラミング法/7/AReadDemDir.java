import java.awt.*;
import java.io.*;
import java.util.Arrays;
import java.util.ArrayList;

public class AReadDemDir {
  private static double pad = -9999;
  private String pdir, mesh;
  private ArrayList <String> dirs = new ArrayList <String> (0),
    files = new ArrayList <String> (0), types = new ArrayList <String> (0);
  private double latN15, latS15, lonW, lonE;
  private double[] lat = new double[1500], lon = new double[2250];
                                           // DEM file x [w->e]  y [n->s]
  private double[][] dem = new double[1500][2250]; //  x [s->n]  y [w->e]
  private byte[][] num = new byte[1500][2250];

  private static boolean isFile (String filename) {
    File file = new File (filename);
    return file.exists () && file.isFile();
  }

  AReadDemDir (String pdir, String mesh) {
    this.pdir = pdir;
    this.mesh = mesh;
    latS15 = (Integer.parseInt (mesh.substring (0,  2))
             +Integer.parseInt (mesh.substring (5,  6))*0.125);
    latN15 = latS15+0.125;
    lonW   = (Integer.parseInt (mesh.substring (2,  4))
             +Integer.parseInt (mesh.substring (6,  7))*0.125)+100;
    lonE   = lonW+0.125;
    double lat0 = latS15/1.5, lon0 = lonW;
/*
  5236-51-56 -> (52+5.5/8)/1.5 = N 35.125  lower :south-most
                (36+1.6/8)+100 = E136.2    left  :west-most
  dem mesh : 0.2arcsec=1/60/60/5degree
  dem value : at the center of the cell
*/
    for (int i = 0; i < 1500; i++) lat[i] = lat0+(i+0.5)/18000;
    for (int j = 0; j < 2250; j++) lon[j] = lon0+(j+0.5)/18000;
    for (int i = 0; i < 1500; i++) Arrays.fill (dem[i], pad);
    for (int i = 0; i < 1500; i++) Arrays.fill (num[i], (byte) -1);
  }

  public void read (String dir, String file) {
    String type = ((dir.length () == 0)
      ? file.substring (0, 6): file.substring (0, 5)).toUpperCase ();
    dirs.add (dir);
    files.add (file);
    types.add (type);
    byte k = (byte) (types.size ()-1);

    if (type.equals ("DEM5A") || type.equals ("DEM5B")) {
      String dirname = pdir + "FG-GML-" + mesh + "-" + dir + "/";
      for (int i = 0; i < 10; i++) {int rx0 = i*150;
      for (int j = 0; j < 10; j++) {int ry0 = j*225;
        String filename =  "FG-GML-" + mesh + "-"
          + Integer.toString (i) + Integer.toString (j) + "-" + file + ".xml";
      if (isFile (dirname + filename)) {
        AReadDem readDem = new AReadDem ();
        Double[][] rDem = readDem.read (dirname + filename);
        Point p = readDem.getStartPoint ();
// 33750 = 225*150             // rDem [150][225]       y:n->s[150] x:w->e[225]
//  private double[][] dem = new double[1500][2250]; // x:s->n      y:w->e
        outer: for (int q = 0, r = p.x+225*p.y; q < 33750; q++, r++) {
          int qx = q/225, qy = q%225;
          if (rDem[qx][qy] == null) break;
          int rx = rx0+(149-r/225), ry = ry0+r%225;
          if ((rDem[qx][qy] != pad) && (dem[rx][ry] == pad)) {
            dem[rx][ry] = rDem[qx][qy]; num[rx][ry] = k;}
        }
System.out.format (" %s %s\n",filename, p.toString());
      }
      }}
    } else if (type.equals ("DEM10B")) {
      String dirname = pdir + "FG-GML-" + mesh.substring (0, 4) + "/";
      String filename = "FG-GML-" + mesh + "-" + file + ".xml";
      if (isFile (dirname + filename)) {
        AReadDem readDem = new AReadDem ();
        Double[][] rDem = readDem.read (dirname + filename);
        Point p = readDem.getStartPoint ();
// 843750 = 1125*750
        outer: for (int q = 0, r = p.x+1125*p.y; q < 843750; q++, r++) {
          int qx = q/1125, qy = q%1125;
          if (rDem[qx][qy] == null) break;
          int rx = 1498-2*(r/1125), ry = 2*(r%1125);
          if (rDem[qx][qy] != pad) {
            if (dem[rx  ][ry  ] == pad) {
              dem[rx  ][ry  ] = rDem[qx][qy]; num[rx  ][ry  ] = k;}
            if (dem[rx+1][ry  ] == pad) {
              dem[rx+1][ry  ] = rDem[qx][qy]; num[rx+1][ry  ] = k;}
            if (dem[rx  ][ry+1] == pad) {
              dem[rx  ][ry+1] = rDem[qx][qy]; num[rx  ][ry+1] = k;}
            if (dem[rx+1][ry+1] == pad) {
              dem[rx+1][ry+1] = rDem[qx][qy]; num[rx+1][ry+1] = k;}
          }
        }
System.out.format (" %s %s\n",filename, p.toString());
      }
    }
/*
{int k0=0,k1=0,k2=0,kk=0;
for (int i = 0; i < 1500; i++) for (int j = 0; j < 2250; j++)
if (num[i][j]==0) k0++; else if (num[i][j]==1) k1++;
else if (num[i][j]==2) k2++; else kk++;
System.out.format ("BBB %d %d %d %d\n",k0,k1,k2,kk);
}
*/
  }

  public double[][] get () {return dem;}

  public double get (int i, int j) {return dem[i][j];}

  public double get (double lat, double lon) {
    double lat15 = lat*1.5;
    return ((latS15 <= lat15) && (lat15 < latN15)
             && (lonW <= lon) && (lon < lonE)) ?
      dem[(int) (((lat*18000)%1500)+0.5)][(int) (((lon*18000)%2250)+0.5)]: pad;
  }

  public byte[][] getNum () {return num;}

  public byte getNum (int i, int j) {return num[i][j];}

  public byte getNum (double lat, double lon) {
    double lat15 = lat*1.5;
    return ((latS15 <= lat15) && (lat15 < latN15)
             && (lonW <= lon) && (lon < lonE)) ?
      num[(int) (((lat*18000)%1500)+0.5)][(int) (((lon*18000)%2250)+0.5)]:
      (byte) -2;
  }
}
