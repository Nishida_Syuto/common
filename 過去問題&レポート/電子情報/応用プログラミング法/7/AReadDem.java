import java.util.*;
import java.awt.*;
import java.awt.geom.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;

public class AReadDem extends DefaultHandler {
  private Double[][] dem;
  private Point startPoint;
  private int nsSize, ewSize;
  private XMLReader reader = null;
  private boolean in;
  private StringBuffer text;

  AReadDem () {
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance ();
      SAXParser parser = factory.newSAXParser ();
      reader = parser.getXMLReader ();
      reader.setContentHandler (this);
      reader.setErrorHandler (this);
    } catch (Throwable t) {
      System.out.format ("Error in ReadDem\n");
      t.printStackTrace ();
    }
  }

  public Double[][] read (String filename) {
    try {
      in = false;
      reader.parse (new InputSource (filename));
    } catch (Throwable t) {
      System.out.format ("Error in ReadDem\n");
      t.printStackTrace ();
    }
    return dem;
  }

  public void setDocumentLocator (Locator l) {
//System.out.format ("reading %s ...\n", l.getSystemId ());
  }

  public void startElement (String uri, String s, String q, Attributes a) {
    if (q.equals ("gml:high") || q.equals ("gml:tupleList")
     || q.equals ("gml:startPoint")) {
      in = true; text = null;
//System.out.format ("start [%s][%s][%s][%s]\n", uri, s, q, a);
    }
  }

  public void endElement (String uri, String s, String q) {
    if (q.equals ("gml:high")) {
      in = false;
      String[] word = text.toString ().replace ("\n", "").trim ().split (" ");
      ewSize = 1+Integer.parseInt (word[0]);
      nsSize = 1+Integer.parseInt (word[1]);
      dem = new Double [nsSize][ewSize];
    } else if (q.equals ("gml:tupleList")) {
      in = false;
      String[] word = text.toString ().trim ().split (",|\n");
      for (int i = 0; i < word.length/2; i++)
        dem[i/ewSize][i%ewSize] = Double.parseDouble (word[i+i+1]);
    } else if (q.equals ("gml:startPoint")) {
      in = false;
      String[] word = text.toString ().replace ("\n", "").trim ().split (" ");
      startPoint = new Point(
        Integer.parseInt (word[0]), Integer.parseInt (word[1]));
    }
  }

  public void characters (char[] buf, int offset, int len) {
    if (in) {
      String s = new String (buf, offset, len);
      if (text == null) text = new StringBuffer (s); else text.append (s);
    }
  }

  public Point getStartPoint () {return startPoint;}

}
