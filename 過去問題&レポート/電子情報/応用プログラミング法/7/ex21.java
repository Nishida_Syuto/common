import java.awt.*;
import javax.swing.*;

public class ex21 {
  static class Ex21 extends JPanel {
    int radius = 200, x0 = 100+radius, y0 = 200+radius;
    float brightness = 0.85f;

    Ex21 () {
      setPreferredSize (APlot.A4);
      setBackground (Color.white);
    }

    public void paintComponent (Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      int rstep = radius/10;
      double pt = 25.4/72;
      a.setCoordinate ((x0-radius)*pt, (y0-radius)*pt, 2*radius*pt, 2*radius*pt,
        -1, 1, -1, 1);
      for (int r = rstep; r <= radius; r += rstep) {
        int astep = (r < 100) ? 10: 5;
      for (int angle  = 0; angle <= 360; angle += astep) {
        float h = angle/360f, s = r/200f;
        g2.setColor (Color.getHSBColor (h, s, brightness));
        a.fillArc (0, 0, r-rstep+2, r, angle+0.5, angle+astep-0.5);
      }}

      g2.setColor (Color.darkGray);
      a.setFont ("SansSerif", 14);
      a.drawString ("HSB Color Chart", -1.08, 1.08);
      g2.setStroke (new BasicStroke (1f,
        BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
      a.setFont ("SansSerif", 12);
      a.drawCircle (0, 0, 1.01*radius);
      for (int i = 0; i < 360; i += 10) {
        double r = Math.toRadians (i), c = Math.cos (r), s = Math.sin (r),
          len = ((i % 30) == 0) ? 9: 7;
        a.drawLineD (1.01*c, 1.01*s, len*c, len*s);
        if ((i % 90) == 0) a.drawString (90+i,
          String.format ("Hue:%5.2f", i/360.0), c, s, 12*c, 12*s, -0.5, -1);
      }
      a.drawXAxis (1f, 0, 1, -1.2, 1, new int[] {2, 5},
        new float[] {1f, 1f, 1f}, new double[] {-10, -8, -6});
      a.drawString ("Saturation: ", 0, -1.2, -10, -20, -1);
      a.drawString ("0.0", 0.0, -1.2, 0, -20, -0.5);
      a.drawString ("0.5", 0.5, -1.2, 0, -20, -0.5);
      a.drawString ("1.0", 1.0, -1.2, 0, -20, -0.5);
      a.drawString (String.format ("Brightness:%6.3f", brightness), -1, -1.2);
    }
  }

  public static void main (String args[]) {
    APage p = new APage (new Ex21 (), "ex21.ps");
  }
}
