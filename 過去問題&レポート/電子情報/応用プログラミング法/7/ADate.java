import java.util.Date;
import java.text.SimpleDateFormat;

public class ADate {
  private static int[][]
    daysTable = {
        {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
        {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}},
    NdayTable = {
        {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
        {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

  public static boolean LeapYear (int year)
    throws IndexOutOfBoundsException {
    // return whether "year" is leap yeap or commom year 
    // return =  false : common year
    //           true  : leap year
    // valid for A.D. 1753-9999
    // IndexOutOfBoundsException : if "year" is out of range
    if (year < 1753 || 9999 < year) 
      throw new IndexOutOfBoundsException
        ("out of range: year = " + Integer.toString (year));
    return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
  }

  public static int JulianDay (int year, int month, int day)
    throws IndexOutOfBoundsException {
    // return Julian-day */
    // 2000 1/1 -> 2451545 */
    // valid for A.D. 1753-9999
    // IndexOutOfBoundsException : if "year", "month", or "day" is out of range
    int epoch = year-2001, d = DayOfYear (year, month, day);
    return 2451545 + (epoch+1)*365 + d 
      + (int) Math.floor (epoch/4.0)
      - (int) Math.floor (epoch/100.0)
      + (int) Math.floor (epoch/400.0);
  }

  public static int NofDays (int year, int month)
    throws IndexOutOfBoundsException {
    //* return the number of days
    //* IndexOutOfBoundsException : if "year", or "month" is out of range
    boolean l = LeapYear (year);
    if (month < 1 || 12 < month) 
      throw new IndexOutOfBoundsException
        ("out of range: month = " + Integer.toString (month));
    return (l) ? NdayTable[1][month]: NdayTable[0][month];
  }

  public static int DayOfYear (int year, int month, int day)
    throws IndexOutOfBoundsException {
    //* return day of year
    //*  1/1 -> 1, 12/31 -> 365 or 366
    //* IndexOutOfBoundsException : if "year", "month", or "day" is out of range
    boolean l = LeapYear (year);
    int nd = NofDays (year, month);
    if (day < 1 || nd < day)
      throw new IndexOutOfBoundsException
        ("out of range: day = " + Integer.toString (day));
    return (l) ? daysTable[1][month-1]+day: daysTable[0][month-1]+day;
  }

  public static int[] DayOfYearToMD (int year, int dayOfYear)
    throws IndexOutOfBoundsException {
    // get "month" and "day" from "year" and "dayOfYear"
    // 1 -> 1/1, 365 -> 12/31 or 12/30
    // IndexOutOfBoundsException : if "year" or "dayOfYear" is out of range
    int d = DayOfYear (year, 12, 31);
    if (dayOfYear < 1 || d < dayOfYear)
      throw new IndexOutOfBoundsException
        ("out of range: dayOfYear = " + Integer.toString (dayOfYear));
    int month = 12;
    while (DayOfYear (year, month, 1) > dayOfYear) --month;
    int day = 1+dayOfYear-DayOfYear (year, month, 1);
    return new int[] {month, day};
  }

  public static int[] JulianDayToYMD (int julianDay)
    throws IndexOutOfBoundsException {
    // get "year", "month" and "day" form "julianDay"
    // valid for 2361331 (1753 1/1) to 5373484 (9999 12/31)
    // 2451545 -> 2000 1/1 */
    // IndexOutOfBoundsException : if "julianDay" is out of range
    if (julianDay < 2361331 || 5373484 < julianDay)
      throw new IndexOutOfBoundsException
        ("out of range: julianDay = " + Integer.toString (julianDay));
    int d = julianDay-2451545;
    if ((d % 146097) == 0) d++;
    d -= (int) Math.floor(d/146097.0)+1;
    d += (int) Math.floor(d/36524.0);
    d -= (int) Math.floor(d/1461.0);
    int year = 2000 + (int) Math.floor(d/365.0);
    int[] md = DayOfYearToMD (year, 1+julianDay-JulianDay(year, 1, 1));
    return new int[] {year, md[0], md[1]};
  }

  public static String nowString () {
    return nowString ("yyyy/MM/dd HH:mm ss''");
  }

  public static String nowString (String pattern) {
    Date now = new Date ();
    return new SimpleDateFormat (pattern).format (now);
  }
}
