import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

public class ex25 {

  public static class Dem {
    String gsiDir = "/roes/sample/app/GSI/";
    double[][] h;
    byte[][] num;
    double lat0, lat1, lon0, lon1;

    Dem (int meshNo) {
      AReadDemDir dem = new AReadDemDir (gsiDir,
        String.format ("%04d-%02d", meshNo/100, meshNo%100));
      dem.read ("DEM5A", "DEM5A-20161001");
      dem.read ("DEM5B", "DEM5B-20161001");
      dem.read ("",     "dem10b-20090201");
      h = dem.get ();
      num = dem.getNum ();
      lat0 = (meshNo/10000+((meshNo/10)%10)/8.0)/1.5;
      lat1 = lat0+1/12.0;
      lon0 = 100+(meshNo/100)%100+(meshNo%10)/8.0;
      lon1 = lon0+1/8.0;
    }
  }


  static class Ex25 extends JPanel {
    double hs = 30, vs = 30, hlen, vlen;
    int meshNo;
    BufferedImage img, img2;
    AColorScheme cs;
    Dem dem;

    Ex25 (int meshNo) {
      this.meshNo = meshNo;
      cs = new AColorScheme ();
      cs.put (1000.0, 0.02, 0.95, 0.6);
      cs.put ( 300.0, 0.05, 0.90, 0.5);
      cs.put ( 200.0, 0.10, 0.85, 0.75);
      cs.put ( 130.0, 0.15, 0.80, 0.30);
      cs.put ( 100.0, 0.20, 0.75, 0.65);
      cs.put (  92.0, 0.30, 0.70, 0.35);
      cs.put (  86.0, 0.35, 0.65, 0.4);
      cs.put (  85.0, 0.40, 0.60, 0.8);
      cs.put (  84.0, 0.45, 0.55, 0.6);
      cs.put (  80.0, 0.50, 0.50, 0.2);
      cs.put (  70.0, 0.55, 0.45, 0.4);
      cs.put (  60.0, 0.60, 0.40, 0.50);
      cs.put (  50.0, 0.65, 0.35, 0.35);
      cs.put (  40.0, 0.70, 0.30, 0.40);
      cs.put (  30.0, 0.75, 0.25, 0.70);
      cs.put (  20.0, 0.80, 0.20, 0.65);
      cs.put (  10.0, 0.85, 0.15, 0.55);
      cs.put (   0.0, 0.90, 0.80, 0.5);

      dem = new Dem (meshNo);
      hlen = 225*Math.cos (Math.toRadians (0.5*(dem.lat0+dem.lat1)));
      vlen = 150;
      img = new BufferedImage (dem.h[0].length, dem.h.length,
        BufferedImage.TYPE_4BYTE_ABGR);
      img2 = new BufferedImage (dem.num[0].length, dem.num.length,
        BufferedImage.TYPE_4BYTE_ABGR);
      for (int i = 0; i < dem.h.length; i++) {
      for (int j = 0; j < dem.h[0].length; j++) {
        float[] hsb = cs.get (dem.h[i][j]);

      int d0 = (dem.num[i][j] != 2) ? 0: 1, d1 = (dem.num[i][j] != 2) ? 1: 2;

        if ((d0 < i) && (i < dem.h.length-d1)
         && (d0 < j) && (j < dem.h[0].length-d1)) {
          double max = Math.max (dem.h[i-d1][j], Math.max (dem.h[i][j-d1],
              Math.max (dem.h[i-d1][j-d1], Math.max (dem.h[i+d1][j-d1],
              Math.max (dem.h[i-d1][j+d1], Math.max (dem.h[i+d1][j+d1],
              Math.max (dem.h[i][j+d1], dem.h[i+d1][j]))))))),
            min = Math.min (dem.h[i-d1][j], Math.min (dem.h[i][j-d1],
              Math.min (dem.h[i-d1][j-d1], Math.min (dem.h[i+d1][j-d1],
              Math.min (dem.h[i-d1][j+d1], Math.min (dem.h[i+d1][j+d1],
              Math.min (dem.h[i][j+d1], dem.h[i+d1][j]))))))),
            mean = 0.125*(
             dem.h[i-d1][j]+dem.h[i][j-d1]+dem.h[i-d1][j-d1]+dem.h[i+d1][j-d1]+
             dem.h[i-d1][j+d1]+dem.h[i+d1][j+d1]+dem.h[i][j+d1]+dem.h[i+d1][j]);
          if (max-min >= 0.25) {
            if (dem.h[i][j] > mean)
              hsb = AColorScheme.interpolate (hsb, new float[] {hsb[0], 0, 1},
                Math.pow (Math.min (1, 0.25*(dem.h[i][j]-mean)/d1), 0.3));
            else if (mean-0.1 > dem.h[i][j])
              hsb = AColorScheme.interpolate (hsb, new float[] {hsb[0], 0, 0},
                Math.pow (Math.min (1, 0.02*(mean-0.1-dem.h[i][j])/d1), 0.4));
          }
        }

        int c = Color.HSBtoRGB (hsb[0], hsb[1], hsb[2]);
        img.setRGB (j, i, c);
        img2.setRGB (j, i, (dem.num[i][j] == 0) ? 0xffeeeeee:
          (dem.num[i][j] == 1) ?  0xffaaaaaa: 0xff222222);
      }}

      setBackground (Color.white);
      setPreferredSize (APlot.A4landscape);
    }

    public void paintComponent (Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      APlot a = new APlot (this, g2);

      a.setFont ("Serif", 13); g2.setColor (Color.black);
      a.drawString (String.format ("%d", meshNo), hs, vs+vlen, 0, 5);

      a.setCoordinate (hs, vs, hlen, vlen,
        dem.lon0, dem.lon1, dem.lat0, dem.lat1);
      a.drawImage (img, dem.lon0, dem.lon1, dem.lat1, dem.lat0);
      a.setCoordinate (hs+hlen+3, vs+0.75*vlen, 0.25*hlen, 0.25*vlen,
        dem.lon0, dem.lon1, dem.lat0, dem.lat1);
      a.drawImage (img2, dem.lon0, dem.lon1, dem.lat1, dem.lat0);

      {double h0 = 0, h1 = 500, hstep = 0.05;
        a.setCoordinate (hs+hlen+15, vs, 20, 0.7*vlen,
          0, 1, h0-0.5*hstep, h1+0.5*hstep);
        a.drawYAxis (0.8f, h0, h1, 1.1, 10, new int[] {2, 5},
          new float[] {0.8f, 0.8f, 0.8f}, new double[] {10, 8, 5});
        a.setFont ("SansSerif", 10); g2.setColor (Color.black);
        for (double h = 10*Math.ceil(h0/10); h <= 10*Math.floor(h1/10); h+=10)
          a.drawString (String.format ("%.0f", h), 1.1, h, 30, -3, -1);
        a.drawString ("[m]", 1.1, h1, 30, 9, -1);
        for (double h = Math.floor (h0); h <= Math.ceil (h1); h += hstep) {
          float[] hsb = cs.get (h);
          g2.setColor (Color.getHSBColor (hsb[0], hsb[1], hsb[2]));
          a.fillRectangle (0, h-0.5*hstep, 1, h+0.5*hstep);
        }
      }
    }
  }

  public static void main (String args[]) {
    boolean landscape = true;
    int meshNo = 533523;

    if (args.length > 0) {
      try {
        meshNo = Integer.parseInt (args[0]);
      } catch (NumberFormatException e) {
        System.err.println ("\"" + args[0] + "\" is not Integer.");
        System.exit(1);
      }
    }
    APage p = new APage (new Ex25 (meshNo),
      String.format ("ex25_%d.ps", meshNo), landscape);
  }
}
