import java.util.*;

public class AColorScheme {
  String name = null;
  TreeMap <Double, float[]> color = new TreeMap <Double, float[]> ();

  public static float[] interpolate (float[] hsb0, float[] hsb1, double d) {
    hsb0[0] = hsb0[0] % 1; hsb1[0] = hsb1[0] % 1;
    if (hsb1[0] > hsb0[0]+0.5) hsb1[0] -= 1;
    if (hsb1[0] < hsb0[0]-0.5) hsb1[0] += 1;
    return  new float[] {
      (float) (hsb0[0]+d*(hsb1[0]-hsb0[0])) % 1,
      (float) (hsb0[1]+d*(hsb1[1]-hsb0[1])),
      (float) (hsb0[2]+d*(hsb1[2]-hsb0[2]))};
  }

  AColorScheme () {
  }

  AColorScheme (String name) {
    this.name = name;
  }

  void put (double height,
    double hue, double saturation, double brightness) {
    color.put (height,
      new float[] {(float) hue, (float) saturation, (float) brightness});
  }

  float[] get (double height) {
    float[] hsb = color.get (height);
    if (hsb == null) {
      Double h0 = color.lowerKey (height), h1 = color.higherKey (height);
      if (h0 == null) hsb = color.get (h1);
      else if (h1 == null) hsb = color.get (h0);
      else {
        float[] hsb0 = color.get (h0), hsb1 = color.get (h1);
        hsb = interpolate (hsb0, hsb1, (height-h0)/(h1-h0));
      }
    }
  //return Color.getHSBColor (hsb[0], hsb[1], hsb[2]);
    //return Color.HSBtoRGB (hsb[0], hsb[1], hsb[2]);
      return hsb;
  }

}
